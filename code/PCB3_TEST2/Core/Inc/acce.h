/*
 * acce.h
 *
 *  Created on: Nov 17, 2021
 *      Author: pokej
 */

#ifndef INC_ACCE_H_
#define INC_ACCE_H_

void adxl_write (uint8_t address, uint8_t value)
{
	uint8_t data_send[2];
	data_send[0] = address|0x40;  // multibyte write
	data_send[1] = value;
	HAL_GPIO_WritePin (ACC_SS_GPIO_Port, ACC_SS_Pin, 0);  // pull the cs pin low
	HAL_SPI_Transmit (&hspi1, data_send, 2, 100);  // write data to register
	HAL_GPIO_WritePin (ACC_SS_GPIO_Port, ACC_SS_Pin, 1);  // pull the cs pin high
}

void adxl_read (uint8_t address, uint8_t* data)
{
	address |= 0x80;  // read operation
	address |= 0x40;  // multibyte read
	HAL_GPIO_WritePin (ACC_SS_GPIO_Port, ACC_SS_Pin, 0);  // pull the pin low
	HAL_SPI_Transmit (&hspi1, &address, 1, 100);  // send address
	HAL_SPI_Receive (&hspi1, data, 6, 100);  // receive 6 bytes data
	HAL_GPIO_WritePin (ACC_SS_GPIO_Port, ACC_SS_Pin, 1);  // pull the pin high
}

uint8_t adxl_read_byte (uint8_t address){
	uint8_t res;
	address |= 0x80;  // read operation
	address |= 0x40;  // multibyte read
	HAL_GPIO_WritePin (ACC_SS_GPIO_Port, ACC_SS_Pin, 0);  // pull the pin low
	HAL_SPI_Transmit (&hspi1, &address, 1, 100);  // send address
	HAL_SPI_Receive (&hspi1, &res, 1, 100);  // receive 6 bytes data
	HAL_GPIO_WritePin (ACC_SS_GPIO_Port, ACC_SS_Pin, 1);  // pull the pin high
	return res;
}
void adxl_init (void)
{
	adxl_write (0x31, 0x09);  // data_format range= +- 4g 10 bit res
	adxl_write (0x2d, 0x00);  // reset all bits
	adxl_write (0x2d, 0x08);  // power_cntl measure and wake up 8hz

	adxl_write (0x2f, 0x00); // int1 for single tap.
	adxl_write (0x2e, 0x80); // interrupt enable single tap enabled

	adxl_write (0x1d, 0x20); // threshold_tap max 16g
	adxl_write (0x21, 0x30); // DUR currently 625 us
	adxl_write (0x22, 0x30); // wait time between taps
	adxl_write (0x23, 0x40); // wait time
	adxl_write (0x2a, 0x0E); // tap_axes suppress + xyz enabled.

	adxl_write (0x2c, 0x0f); // set date output data rate to 1600Hz
	adxl_write (0x1E, 0x00); // clearing all the offsets
	adxl_write (0x1F, 0x00);
	adxl_write (0x20, 0x00);
}

void acc_calibration(int32_t* a0, int32_t* a1, int16_t* temp1, int16_t* v1, int16_t* v0, int16_t* v00, int16_t* delx, int16_t* offsets, int16_t* v_zero_count, int16_t t){
  for(int i=0; i<3; i++){

    a0[i] = 0;
    a1[i] = 0;
    temp1[i] = 0;
    v1[i] = 0;
    v0[i] = 0;
    v00[i] = 0;
    delx[i] = 0;
    offsets[i] = 0;
    v_zero_count[i] = 0;
  }
  uint8_t data[6];
  for(int i=0; i<t; i++){
    adxl_read(0x32, data);
    offsets[0] += ((data[1]<<8)|data[0]);
    offsets[1] += ((data[3]<<8)|data[2]);
    offsets[2] += ((data[5]<<8)|data[4]);
  }
  for(int i=0; i<3; i++){
    offsets[i] /= t;
  }
}
#endif /* INC_ACCE_H_ */

/*
 * gyro.h
 *
 *  Created on: Nov 17, 2021
 *      Author: pokej
 */

#ifndef INC_GYRO_H_
#define INC_GYRO_H_

void gyro_write (uint8_t ss, uint8_t address, uint8_t value)
{
	uint8_t data_send[2];
//	address &= 0x7F;
	data_send[0] = address;
	data_send[1] = value;
	if (ss ==1)
		HAL_GPIO_WritePin (GYRO_SS1_GPIO_Port, GYRO_SS1_Pin, 0);  // pull the pin low
	else if (ss == 2)
		HAL_GPIO_WritePin (GYRO_SS2_GPIO_Port, GYRO_SS2_Pin, 0);  // pull the pin low
	else
		HAL_GPIO_WritePin (GYRO_SS3_GPIO_Port, GYRO_SS3_Pin, 0);  // pull the pin low
	HAL_SPI_Transmit (&hspi2, data_send, 2, 100);  // write data to register
	if (ss ==1)
		HAL_GPIO_WritePin (GYRO_SS1_GPIO_Port, GYRO_SS1_Pin, 1);  // pull the pin high
	else if (ss == 2)
		HAL_GPIO_WritePin (GYRO_SS2_GPIO_Port, GYRO_SS2_Pin, 1);  // pull the pin high
	else
		HAL_GPIO_WritePin (GYRO_SS3_GPIO_Port, GYRO_SS3_Pin, 1);  // pull the pin high
}

void gyro_read (uint8_t ss, uint8_t address, uint8_t* data)
{
	address |= 0x80;  // read operation
	if (ss ==1)
		HAL_GPIO_WritePin (GYRO_SS1_GPIO_Port, GYRO_SS1_Pin, 0);  // pull the pin low
	else if (ss == 2)
		HAL_GPIO_WritePin (GYRO_SS2_GPIO_Port, GYRO_SS2_Pin, 0);  // pull the pin low
	else
		HAL_GPIO_WritePin (GYRO_SS3_GPIO_Port, GYRO_SS3_Pin, 0);  // pull the pin low

	HAL_SPI_Transmit (&hspi2, &address, 1, 100);  // send address
	HAL_SPI_Receive (&hspi2, data, 4, 100);  // receive 4 bytes data

	if (ss ==1)
		HAL_GPIO_WritePin (GYRO_SS1_GPIO_Port, GYRO_SS1_Pin, 1);  // pull the pin high
	else if (ss == 2)
		HAL_GPIO_WritePin (GYRO_SS2_GPIO_Port, GYRO_SS2_Pin, 1);  // pull the pin high
	else
		HAL_GPIO_WritePin (GYRO_SS3_GPIO_Port, GYRO_SS3_Pin, 1);  // pull the pin high
}

void gyro_init (void)
{
	HAL_GPIO_WritePin (GYRO_SS1_GPIO_Port, GYRO_SS1_Pin, 1);  // pull the pin high
	HAL_GPIO_WritePin (GYRO_SS2_GPIO_Port, GYRO_SS2_Pin, 1);  // pull the pin high
	HAL_GPIO_WritePin (GYRO_SS3_GPIO_Port, GYRO_SS3_Pin, 1);
	for(int i=1; i<4; i++){
		gyro_write(i, 0x6b, 0x80);
		HAL_Delay(200);
		gyro_write(i, 0x1b, 0x18);
		gyro_write(i, 0x38, 0x01);
		gyro_write(i, 0x37, 0x30);
	}
}

void gyro_read_all(int16_t* gyro_x, int16_t* gyro_y){
	uint8_t gyro_data[4];
	gyro_read(1, 0x43, gyro_data);
	gyro_x[0] = ((gyro_data[0]<<8)|gyro_data[1]);
	gyro_y[0] = ((gyro_data[2]<<8)|gyro_data[3]);
	gyro_read(2, 0x43, gyro_data);
	gyro_x[1] = ((gyro_data[0]<<8)|gyro_data[1]);
	gyro_y[1] = ((gyro_data[2]<<8)|gyro_data[3]);
	gyro_read(3, 0x43, gyro_data);
	gyro_x[2] = ((gyro_data[0]<<8)|gyro_data[1]);
	gyro_y[2] = ((gyro_data[2]<<8)|gyro_data[3]);
}

void gyro_clickop(uint8_t ss, int16_t* gyro_x, int16_t* c_diff_time, int16_t* r_diff_time, int16_t* click_time, int16_t* release_time, int16_t* click_flag, int16_t* release_flag, int16_t* active_flag){
	int16_t cx;
	uint8_t Test[80];
	int i = ss-1;
	// making gyroscope decision
	if (gyro_x[i] <= -15000 && i != 0){
		click_flag[i] = 1;
	}
	if (gyro_x[i] >= 15000 && i != 0){
		release_flag[i] = 1;
	}
	if (gyro_x[0] <= -25000 && i != 0){
		click_flag[0] = 1;
	}
	if (gyro_x[0] >= 25000 && i != 0){
		release_flag[0] = 1;
	}

	// gyroscope operation
	c_diff_time[i] = (click_time[i] == 0) ? 1100 : (__HAL_TIM_GET_COUNTER(&htim2) + rollover_cnt * 65535 - click_time[i]);
	if (click_flag[i] && c_diff_time[i]>300){

		active_flag[i] = 1;
		click_flag[i] = 0;
		click_time[i] = __HAL_TIM_GET_COUNTER(&htim2) + rollover_cnt * 65535;

	}
	r_diff_time[i] = (release_time[i] == 0) ? 1100 : (__HAL_TIM_GET_COUNTER(&htim2) + rollover_cnt * 65535 - release_time[i]);
	if (release_flag[i] && r_diff_time[i]>300){
		active_flag[i] = 2;
		release_flag[i] = 0;
		release_time[i] = __HAL_TIM_GET_COUNTER(&htim2) + rollover_cnt * 65535;

	}
	click_flag[i] = 0;
	release_flag[i] = 0;
}

void calibration_flag_update(uint8_t ss, int16_t* gyro_x, int16_t* c_diff_time, int16_t* r_diff_time, int16_t* click_time, int16_t* release_time, int16_t* click_flag, int16_t* release_flag){
	int16_t cx;
	uint8_t Test[80];
	int i = ss-1;
	// making gyroscope decision
	if (gyro_x[i] <= -18000 && abs(gyro_x[1])<8500 && abs(gyro_x[2])<8500){
		gyro_x[i] = 0;
		click_flag[i] = 1;
	}

	// gyroscope operation
	c_diff_time[i] = (click_time[i] == 0) ? 1100 : (__HAL_TIM_GET_COUNTER(&htim2) + rollover_cnt * 65535 - click_time[i]);
	if (click_flag[i] && c_diff_time[i]>500){
		if(calibration_flag==0){
			calibration_flag = 1;
		}
		click_flag[i] = 0;
		click_time[i] = __HAL_TIM_GET_COUNTER(&htim2) + rollover_cnt * 65535;
	}
	click_flag[i] = 0;
}

void gyro_calibration(int16_t* gyro_x, int16_t* gyro_y, int16_t* click_time, int16_t* release_time, int16_t* click_flag, int16_t* release_flag, int16_t* active_flag){
	for(int i=0; i<3; i++){
		gyro_x[i] = 0;
		gyro_y[i] = 0;
		click_time[i] = 0;
		release_time[i] = 0;
		click_flag[i] = 0;
		release_flag[i] = 0;
		active_flag[i] = 0;

	}

}

#endif /* INC_GYRO_H_ */

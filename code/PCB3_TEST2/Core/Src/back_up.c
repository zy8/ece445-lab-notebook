/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

#include <stdio.h>
#include <stdlib.h>

//System global handler
SPI_HandleTypeDef hspi1;
SPI_HandleTypeDef hspi2;
TIM_HandleTypeDef htim2;
UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;

//System Init
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_SPI1_Init(void);
static void MX_SPI2_Init(void);
static void MX_TIM2_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_USART2_UART_Init(void);
uint32_t rollover_cnt = 0;
uint8_t mouse[] = {0xFD, 0x05, 0x02, 0x00, 0x00, 0x00, 0x00};
uint8_t vol_up[] = {0xFD, 0x03, 0x03, 0x10, 0x00};
uint8_t vol_down[] = {0xFD, 0x03, 0x03, 0x20, 0x00};
uint8_t vol_release[] = {0xFD, 0x03, 0x03, 0x00, 0x00};
uint8_t zoom_in[] = {0xFD, 0x09, 0x01, 0x01, 0x00, 0x2E, 0x00, 0x00, 0x00, 0x00, 0x00};
uint8_t zoom_out[] = {0xFD, 0x09, 0x01, 0x01, 0x00, 0x2D, 0x00, 0x00, 0x00, 0x00, 0x00};
uint8_t ctrl_release[] = {0xFD, 0x09, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
uint8_t calibration_flag = 0;

#include "acce.h"
#include "gyro.h"
int main(void)
{
  HAL_Init();
  SystemClock_Config();
  MX_GPIO_Init();
  MX_SPI1_Init();
  MX_SPI2_Init();
  MX_TIM2_Init();
  HAL_TIM_Base_Start_IT(&htim2);
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();

  //makes sure BT is not reset
  HAL_GPIO_WritePin(BT_RST_GPIO_Port, BT_RST_Pin, 1);

  //ACCE init
  HAL_GPIO_WritePin (ACC_SS_GPIO_Port, ACC_SS_Pin, 1);
  adxl_init();
  uint8_t data[6];
  int32_t a0[3];
  int32_t a1[3];
  int16_t temp1[3];
  int16_t v00[3];
  int16_t v0[3];
  int16_t v1[3];
  int16_t delx[3];
  int16_t offsets[3];
  int16_t a_threshold[3] = {10, 10, 20};
  int16_t temp_threshold = 35;
  int16_t a_avg_count = 0;
  int16_t v_zero_count[3];
  int16_t v_zero_threshold = 128;
  int16_t a_bt_count = 0;
  acc_calibration(a0, a1, temp1, v1, v0, v00, delx, offsets, v_zero_count, 100);

  //GYRO Init
  gyro_init();
  int16_t gyro_x[3], gyro_y[3];
  int16_t c_diff_time[3], r_diff_time[3];
  int16_t click_time[3] = {0,0,0};
  int16_t release_time[3] = {0,0,0};
  int16_t click_flag[3] = {0,0,0};
  int16_t release_flag[3] = {0,0,0};
  int16_t active_flag[3] = {0, 0, 0};
  int16_t vol_up_count = 10;
  int16_t vol_down_count = 10;
  int16_t zoom_in_count = 30;
  int16_t zoom_out_count = 30;
  int16_t scroll_count = 0;
  gyro_calibration(gyro_x, gyro_y, click_time, release_time, click_flag, release_flag, active_flag);

  //USART1 (aka print) init
  int16_t cx;
  uint8_t Test[80];

  while (1)
  {
   adxl_read(0x32, data);
   temp1[0] = ((data[1]<<8)|data[0]);
   temp1[1] = ((data[3]<<8)|data[2]);
   temp1[2] = ((data[5]<<8)|data[4]);
   a1[0] += temp1[0];
   a1[1] += temp1[1];
   a1[2] += temp1[2];


   a_avg_count++;
   a_bt_count++;

   if(a_avg_count == 127){
     for(int i=0; i<3; i++){
       a1[i] = a1[i] / 128 - offsets[i];
      if ( abs(a1[i] - a0[i]) >= 180 && abs(a0[i]) > 20 )
       		a1[i] = a0[i];
       if (abs(a1[i]) < a_threshold[i]){
 			  a1[i] = 0;
 		  }

       v1[i] = v0[i] + a0[i] + ((a1[i] - a0[i])>>1);
       if (abs(v1[i]) < 40){
       	v1[i] = 0;
       }
       if(v1[i]!=0 && v1[i]==v0[i] && v0[i] == v00[i]){
   			v1[i] = 0;
   		}
       delx[i] = v0[i] + ((v1[i] - v0[i])>>1);
       a0[i] = a1[i];
       v00[i] = v0[i];
       v0[i] = v1[i];

     }


     a_avg_count = 0;
   }
    if(a_bt_count==127){
      a_bt_count = 0;
//       cx = snprintf(Test, 50, "\r\ndata:%d %d %d", a1[1], v1[1], delx[1]);
//       HAL_UART_Transmit(&huart1,Test,cx,10);
      gyro_read_all(gyro_x, gyro_y);
      gyro_clickop(1, gyro_x, c_diff_time, r_diff_time, click_time, release_time, click_flag, release_flag, active_flag);
      gyro_clickop(2, gyro_x, c_diff_time, r_diff_time, click_time, release_time, click_flag, release_flag, active_flag);
      gyro_clickop(3, gyro_x, c_diff_time, r_diff_time, click_time, release_time, click_flag, release_flag, active_flag);

      if(a1[0]>=220){
        cx = snprintf(Test, 50, "\r\ncalibration");
        HAL_UART_Transmit(&huart1,Test,cx,10);
        HAL_Delay(5000);
        acc_calibration(a0, a1, temp1, v1, v0, v00, delx, offsets, v_zero_count, 100);
        a_avg_count = 0;
        a_bt_count = 0;
        gyro_calibration(gyro_x, gyro_y, click_time, release_time, click_flag, release_flag, active_flag);
		vol_up_count = 10;
		vol_down_count = 10;
		zoom_in_count = 30;
		zoom_out_count = 30;
		scroll_count = 0;
//        HAL_NVIC_SystemReset();
        HAL_Delay(1000);
        continue;
      }

      if(active_flag[0] == 1){
        mouse[4] = 0;
        mouse[5] = 0;
        if(active_flag[1] == 1){
          // mouse[6] = -(abs(gyro_x[1])-15000)/1700;
          mouse[6] = -1;
          if(scroll_count==15){
            HAL_UART_Transmit(&huart2,mouse,sizeof(mouse),10);
            scroll_count = 0;
          }
          scroll_count++;
          mouse[6] = 0;
          if(gyro_x[1]>5000){
            active_flag[1] = 0;
          }
        }
        else if(active_flag[1] == 2){
          mouse[6] = 1;
          if(scroll_count==15){
            HAL_UART_Transmit(&huart2,mouse,sizeof(mouse),10);
            scroll_count = 0;
          }
          scroll_count++;
          mouse[6] = 0;
          if(gyro_x[1]<-5000){
            active_flag[1] = 0;
          }
        }

        if(a1[1]<=-180){
          if(vol_down_count==10){
            HAL_UART_Transmit(&huart2,vol_down,sizeof(vol_down),10);
            HAL_UART_Transmit(&huart2,vol_release,sizeof(vol_release),10);
            vol_down_count = 0;
          }
          vol_down_count++;

        } else if(a1[1]>=180){
          if(vol_up_count==10){
            HAL_UART_Transmit(&huart2,vol_up,sizeof(vol_up),10);
            HAL_UART_Transmit(&huart2,vol_release,sizeof(vol_release),10);
            vol_up_count = 0;
          }
          vol_up_count++;
        }
        // zoom in and out
        if(a1[0]<=-100){
          if(zoom_in_count==30){
            HAL_UART_Transmit(&huart2,zoom_in,sizeof(zoom_in),10);
            HAL_UART_Transmit(&huart2,ctrl_release,sizeof(ctrl_release),10);
            zoom_in_count = 0;
          }
          zoom_in_count++;
        } else if(a1[0]>=100){
          if(zoom_out_count==30){
            HAL_UART_Transmit(&huart2,zoom_out,sizeof(zoom_out),10);
            HAL_UART_Transmit(&huart2,ctrl_release,sizeof(ctrl_release),10);
            zoom_out_count = 0;
          }
          zoom_out_count++;
        }
        cx = snprintf(Test, 80, "\r\ndata:%d %d %d %d %d %d", active_flag[0], active_flag[1], active_flag[2], gyro_x[0], gyro_x[1], a1[1]);
               HAL_UART_Transmit(&huart1,Test,cx,10);
        continue;
      } else {
        if(active_flag[1] == 1){
          mouse[3] |= 0x01;
        }
        else if(active_flag[1] == 2){
          mouse[3] &= 0xFE;
        }
        else if(active_flag[2] == 1){
          mouse[3] |= 0x02;
        }
        else if (active_flag[2] == 2){
          mouse[3] &= 0xFD;
        }
        if(abs(a1[1])>=50) {
        	mouse[4] = 5*(a1[1]/abs(a1[1]));
        }
        else {
        	mouse[4] = 0;
        }
        if(abs(a1[0])>=50) {
            mouse[5] = -5*(a1[0]/abs(a1[0]));
        }
        else {
            mouse[5] = 0;
        }
      }


      active_flag[1] = 0;
      active_flag[2] = 0;
      cx = snprintf(Test, 80, "\r\ndata:%d %d %d %d %d %d", active_flag[0], active_flag[1], active_flag[2], gyro_x[0], gyro_x[1], a1[1]);
                     HAL_UART_Transmit(&huart1,Test,cx,10);

 	  HAL_UART_Transmit(&huart2,mouse,sizeof(mouse),10);
 	  mouse[6] = 0;
    }

  }
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    rollover_cnt++;
}
/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL3;
  RCC_OscInitStruct.PLL.PLLDIV = RCC_PLL_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  HAL_RCC_MCOConfig(RCC_MCO1, RCC_MCO1SOURCE_SYSCLK, RCC_MCODIV_1);
}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_HIGH;
  hspi1.Init.CLKPhase = SPI_PHASE_2EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief SPI2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI2_Init(void)
{

  /* USER CODE BEGIN SPI2_Init 0 */

  /* USER CODE END SPI2_Init 0 */

  /* USER CODE BEGIN SPI2_Init 1 */

  /* USER CODE END SPI2_Init 1 */
  /* SPI2 parameter configuration*/
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_HIGH;
  hspi2.Init.CLKPhase = SPI_PHASE_2EDGE;
  hspi2.Init.NSS = SPI_NSS_SOFT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI2_Init 2 */

  /* USER CODE END SPI2_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 24575;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 65535;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 921600;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(ACC_SS_GPIO_Port, ACC_SS_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LED_Pin|BT_RST_Pin|GYRO_SS1_Pin|GYRO_SS2_Pin
                          |GYRO_SS3_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : ACC_SS_Pin */
  GPIO_InitStruct.Pin = ACC_SS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(ACC_SS_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LED_Pin BT_RST_Pin GYRO_SS1_Pin GYRO_SS2_Pin
                           GYRO_SS3_Pin */
  GPIO_InitStruct.Pin = LED_Pin|BT_RST_Pin|GYRO_SS1_Pin|GYRO_SS2_Pin
                          |GYRO_SS3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : PA8 */
  GPIO_InitStruct.Pin = GPIO_PIN_8;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF0_MCO;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

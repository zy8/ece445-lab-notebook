/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
SPI_HandleTypeDef hspi2;

TIM_HandleTypeDef htim2;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_SPI2_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_TIM2_Init(void);
uint32_t rollover_cnt = 0;
uint8_t data[4];

void gyro_write (uint8_t ss, uint8_t address, uint8_t value)
{
	uint8_t data_send[2];
	data_send[0] = address;
	data_send[1] = value;
	if (ss ==1)
		HAL_GPIO_WritePin (GYRO_SS1_GPIO_Port, GYRO_SS1_Pin, 0);  // pull the pin low
	else if (ss == 2)
		HAL_GPIO_WritePin (GYRO_SS2_GPIO_Port, GYRO_SS2_Pin, 0);  // pull the pin low
	else
		HAL_GPIO_WritePin (GYRO_SS3_GPIO_Port, GYRO_SS3_Pin, 0);  // pull the pin low
	HAL_SPI_Transmit (&hspi2, data_send, 2, 100);  // write data to register
	if (ss ==1)
		HAL_GPIO_WritePin (GYRO_SS1_GPIO_Port, GYRO_SS1_Pin, 1);  // pull the pin high
	else if (ss == 2)
		HAL_GPIO_WritePin (GYRO_SS2_GPIO_Port, GYRO_SS2_Pin, 1);  // pull the pin high
	else
		HAL_GPIO_WritePin (GYRO_SS3_GPIO_Port, GYRO_SS3_Pin, 1);  // pull the pin high
}

void gyro_read (uint8_t ss, uint8_t address)
{
	address |= 0x80;  // read operation
	if (ss ==1)
		HAL_GPIO_WritePin (GYRO_SS1_GPIO_Port, GYRO_SS1_Pin, 0);  // pull the pin low
	else if (ss == 2)
		HAL_GPIO_WritePin (GYRO_SS2_GPIO_Port, GYRO_SS2_Pin, 0);  // pull the pin low
	else
		HAL_GPIO_WritePin (GYRO_SS3_GPIO_Port, GYRO_SS3_Pin, 0);  // pull the pin low

	HAL_SPI_Transmit (&hspi2, &address, 1, 100);  // send address
	HAL_SPI_Receive (&hspi2, data, 4, 100);  // receive 4 bytes data

	if (ss ==1)
		HAL_GPIO_WritePin (GYRO_SS1_GPIO_Port, GYRO_SS1_Pin, 1);  // pull the pin high
	else if (ss == 2)
		HAL_GPIO_WritePin (GYRO_SS2_GPIO_Port, GYRO_SS2_Pin, 1);  // pull the pin high
	else
		HAL_GPIO_WritePin (GYRO_SS3_GPIO_Port, GYRO_SS3_Pin, 1);  // pull the pin high
}
//
//int8_t gyro_read_byte (uint8_t address){
//	int8_t res;
//	address |= 0x80;  // read operation
//	HAL_GPIO_WritePin (GYRO_SS1_GPIO_Port, GYRO_SS1_Pin, 0);  // pull the pin low
//	HAL_SPI_Transmit (&hspi2, &address, 1, 100);  // send address
//	HAL_SPI_Receive (&hspi2, &res, 1, 100);  // receive 6 bytes data
//	HAL_GPIO_WritePin (GYRO_SS1_GPIO_Port, GYRO_SS1_Pin, 1);  // pull the pin high
//	return res;
//}
void gyro_init (void)
{
	for(int i=1; i<4; i++){
		gyro_write(i, 0x6b, 0x80);
		gyro_write(i, 0x1b, 0x18);
	}


}

void op_click(uint8_t parameter){

}
int main(void)
{
  HAL_Init();

  SystemClock_Config();
  MX_GPIO_Init();
  MX_SPI2_Init();
  MX_USART2_UART_Init();
  MX_TIM2_Init();
  HAL_TIM_Base_Start_IT(&htim2);
  uint32_t click_time = 0;
  uint32_t release_time = 0;
  uint32_t diff_time;
  uint32_t r_diff_time;

  uint32_t g3_click_time = 0;
  uint32_t g3_release_time = 0;
  uint32_t g3_diff_time;
  uint32_t g3_r_diff_time;

  int16_t gyro_1x, gyro_1y, gyro_2x, gyro_2y, gyro_3x, gyro_3y;
  uint8_t g1_click_flag = 0;
  uint8_t g1_release_flag = 0;
  uint8_t g3_click_flag = 0;
  uint8_t g3_release_flag = 0;
  gyro_init();

  uint8_t toggle_cnt = 1;
  HAL_GPIO_WritePin (GYRO_SS2_GPIO_Port, GYRO_SS2_Pin, 0);
  uint8_t mouse[] = {0xFD, 0x05, 0x02, 0x00, 0x00, 0x00, 0x00};

  while (1)
  {
	  // read gyroscope data
	  gyro_read(1, 0x43);
	  gyro_1x = ((data[0]<<8)|data[1]);
	  gyro_1y = ((data[2]<<8)|data[3]);
	  gyro_read(3, 0x43);
	  gyro_3x = ((data[0]<<8)|data[1]);
	  gyro_3y = ((data[2]<<8)|data[3]);

	  // making gyroscope decision
	  if (gyro_1x >= 14000 ){
		  g1_click_flag = 1;
	  }
	  if (gyro_1x <= -14000 ){
		  g1_release_flag = 1;
	  }

	  // gyroscope operation
	  diff_time = (click_time == 0) ? 1100 : (__HAL_TIM_GET_COUNTER(&htim2) + rollover_cnt * 65535 - click_time);
	  if (g1_click_flag && diff_time>300){
		  mouse[3] |= 0x02;
		  HAL_UART_Transmit(&huart2,mouse,sizeof(mouse),10);
		  HAL_GPIO_WritePin(GYRO_SS2_GPIO_Port, GYRO_SS2_Pin, toggle_cnt%2);
		  toggle_cnt++;
		  g1_click_flag = 0;
		  click_time = __HAL_TIM_GET_COUNTER(&htim2) + rollover_cnt * 65535;
	  }
	  r_diff_time = (release_time == 0) ? 1100 : (__HAL_TIM_GET_COUNTER(&htim2) + rollover_cnt * 65535 - release_time);
	  if (g1_release_flag && r_diff_time>300){
		  mouse[3] &= 0xFD;
		  HAL_UART_Transmit(&huart2,mouse,sizeof(mouse),10);
		  HAL_GPIO_WritePin(GYRO_SS2_GPIO_Port, GYRO_SS2_Pin, toggle_cnt%2);
		  toggle_cnt++;
		  g1_release_flag = 0;
		  release_time = __HAL_TIM_GET_COUNTER(&htim2) + rollover_cnt * 65535;
	  }
	  g1_click_flag = 0;
	  g1_release_flag = 0;


	  if (gyro_3x >= 14000 ){
		  g3_click_flag = 1;
	  }
	  if (gyro_3x <= -14000 ){
		  g3_release_flag = 1;
	  }

	  // gyroscope operation
	  g3_diff_time = (g3_click_time == 0) ? 1100 : (__HAL_TIM_GET_COUNTER(&htim2) + rollover_cnt * 65535 - g3_click_time);
	  if (g3_click_flag && g3_diff_time>300){
		  mouse[3] |= 0x01;
		  HAL_UART_Transmit(&huart2,mouse,sizeof(mouse),10);
//		  HAL_GPIO_WritePin(GYRO_SS2_GPIO_Port, GYRO_SS2_Pin, toggle_cnt%2);
//		  toggle_cnt++;
		  g3_click_flag = 0;
		  g3_click_time = __HAL_TIM_GET_COUNTER(&htim2) + rollover_cnt * 65535;
	  }
	  g3_r_diff_time = (g3_release_time == 0) ? 1100 : (__HAL_TIM_GET_COUNTER(&htim2) + rollover_cnt * 65535 - g3_release_time);
	  if (g3_release_flag && g3_r_diff_time>300){
		  mouse[3] &= 0xFE;
		  HAL_UART_Transmit(&huart2,mouse,sizeof(mouse),10);
//		  HAL_GPIO_WritePin(GYRO_SS2_GPIO_Port, GYRO_SS2_Pin, toggle_cnt%2);
//		  toggle_cnt++;
		  g3_release_flag = 0;
		  g3_release_time = __HAL_TIM_GET_COUNTER(&htim2) + rollover_cnt * 65535;
	  }
	  g3_click_flag = 0;
	  g3_release_flag = 0;
  }
}
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    rollover_cnt++;
}
/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL3;
  RCC_OscInitStruct.PLL.PLLDIV = RCC_PLL_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief SPI2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI2_Init(void)
{

  /* USER CODE BEGIN SPI2_Init 0 */

  /* USER CODE END SPI2_Init 0 */

  /* USER CODE BEGIN SPI2_Init 1 */

  /* USER CODE END SPI2_Init 1 */
  /* SPI2 parameter configuration*/
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_HIGH;
  hspi2.Init.CLKPhase = SPI_PHASE_2EDGE;
  hspi2.Init.NSS = SPI_NSS_SOFT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI2_Init 2 */

  /* USER CODE END SPI2_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 24575;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 65535;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GYRO_SS2_Pin|GYRO_SS1_Pin|GYRO_SS3_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : GYRO_SS2_Pin GYRO_SS1_Pin GYRO_SS3_Pin */
  GPIO_InitStruct.Pin = GYRO_SS2_Pin|GYRO_SS1_Pin|GYRO_SS3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

# ECE445 Jeff Chang (kcchang3) Notebook
[[_TOC_]]
## Sep 18, 2021

I made a calendar of plan for this project, I will update it here soon (TODO)

## Sep 19, 2021

Our team ordered some parts from digikey for testing this week:
1. Gyroscope (https://www.digikey.com/product-detail/en/stmicroelectronics/STEVAL-MKI136V1/497-14894-ND/4901476)
2. Accelerometer (https://www.digikey.com/product-detail/en/analog-devices-inc/EVAL-ADXL335Z/EVAL-ADXL335Z-ND/1995483)
3. Arm Cortex M3 development board (https://www.digikey.com/product-detail/en/stmicroelectronics/NUCLEO-L152RE/497-14363-ND/4695528)
TODO(add pictures)

## Sep 20, 2021
Installed the IDE for the development board
Ran the first LED blink program
Need to continue with the tutorial on url (https://wiki.st.com/stm32mcu/wiki/Category:STM32_step_by_step)
Need to read more of the datasheet of the stm32mcu

## Sep 21, 2021 (Sensor implementation on development board)
Today after finishing the tutorials on the Stm32 chip, I implemented the first sensor reading algorithm using the Stm tools. I handed off the board to Dom so he can play with the development board a little as well.
1. Tutorial: I followed through the Blink LED tutorial and created the first example on the development board. I first marked the GPIO pins and configurations on STM32CubeMX (a GUI for setting configuration and generating bare-bone code), then wrote a simple while loop to toggle the LED GPIO pin.

<p align="center">
  <img src="./screenshots/First_Implementation.PNG" width="800" >
</p>


2. I started a new STM32Cube project to read in the Accelerometer data for the first time. Again, I set 3 pins on the microcontroller to be GPIO Analog/ADC input, configured the ADC sampling rate and channels, and generated the framework code from the MX software. (I had to refer to the pin layout documentation for this).
  - PA0, PA1, PA4 were used to take in the x, y, z data from the accelerometer
  - Continuous Conversion Mode and Scan Mode were enabled

Since the chip only has 1 ADC, I separated the initialize code into 3 separate functions for each channel. Then I called the ADC on each channel consecutively to read in the values. (I only showed the necessary code)
```
void ADC_Select_CH0 (void) {
    ADC_ChannelConfTypeDef sConfig = {0};
	  sConfig.Channel = ADC_CHANNEL_0;
	  sConfig.Rank = ADC_REGULAR_RANK_1;
	  sConfig.SamplingTime = ADC_SAMPLETIME_4CYCLES;
	  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
	  {
	    Error_Handler();
	  }
}
void ADC_Select_CH1 (void) {
    ADC_ChannelConfTypeDef sConfig = {0};
	  sConfig.Channel = ADC_CHANNEL_1;
	  sConfig.Rank = ADC_REGULAR_RANK_1;
	  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
	  {
	    Error_Handler();
	  }
}
void ADC_Select_CH4 (void) {
    ADC_ChannelConfTypeDef sConfig = {0};
	  sConfig.Channel = ADC_CHANNEL_4;
	  sConfig.Rank = ADC_REGULAR_RANK_1;
	  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
	  {
	    Error_Handler();
	  }
}
while (1)
{
  ADC_Select_CH0();
  HAL_ADC_Start(&hadc);
  HAL_ADC_PollForConversion(&hadc, 1);
  x = HAL_ADC_GetValue(&hadc);
  HAL_ADC_Stop(&hadc);

  ADC_Select_CH1();
  HAL_ADC_Start(&hadc);
  HAL_ADC_PollForConversion(&hadc, 1);
  y = HAL_ADC_GetValue(&hadc);
  HAL_ADC_Stop(&hadc);

  ADC_Select_CH4();
  HAL_ADC_Start(&hadc);
  HAL_ADC_PollForConversion(&hadc, 1);
  z = HAL_ADC_GetValue(&hadc);
  HAL_ADC_Stop(&hadc);
}
```
The last part of this First_Implementation was to output the read data into a serial port for us to further develop the algorithm for data extraction. I followed this website (https://shawnhymel.com/1873/how-to-use-printf-on-stm32/) to accomplish this functionality. I overloaded some systemcalls on the microcontroller and was able to print the data on a connected serial console on my computer.

## Sep 26, 2021 (Sensor Update/PCB/Design Doc)
### Sensor Update:
Zhiyuan researched that digital sensors tend to produce less noise compared with traditional analog sensors. We decided to switch to digital sensors for both accelerometer and gyroscope, which communicate with the MCU via SPI. Zhiyuan drew the PCB schematic for the accelerometer:

<p align="center">
  <img src="./screenshots/acce_digital.png" width="600" >
</p>

and Gyroscope

<p align="center">
  <img src="./screenshots/gyro_digital.png" width="600" >
</p>

### PCB Update:
Since we have to finish the PCB design for the first PCB order and for design document. I was in charge of connecting all the parts togeter into a complete PCB design. So I read the entire MCU datasheet, communicated with Zhiyuan and Yayati to complete the first implementation of the full PCB. It includes several parts such as MCU, voltage regulators, sensors, and bluetooth module. I also figured out what pins to connect to the MCU for external clock (which is important when programming the MCU through USB), what pins to connect to the MCU for USB connection, and SPI/UART pins as well. This is the PCB schematic as of right now.
![PCB1](./screenshots/PCB1.png)

### Design Document Check:
We completed the main parts of our Design Document and is ready for the design document check. We wrote complete Introduction, most of the design (except sensors and bluetooth), and cost/schedule. I was in charged of writing the MCU part, introduction, and cost/schedule. TA said we are on the right track on the right pace. We will just need to complete the design document the next week and get the PCB ready for the first order.

## October 3rd, 2021 (Design Document/Sensor Update)
### Design Document
This week we mainly focused on completing the design document. Our design document check was great and received a lot good feedbacks. We incorporate them into the document (such as writing about bluetooth latency and specifying a particular part for our tolerance analysis). Zhiyuan found a good paper explaining the algorithm to calculate velocity and position data from the accelerometer. Him and I completed the tolerance analysis on that part of the design document. I also checked over the entire design document and gave feedback to each of the member to improve our writing. It seems like our design is mostly ready and we need to complete the PCB design and start ordering parts by now.
### Sensor Update
It seems like the sensor we chose is out of stock, so we will be updating this information on our design in the following days.

## October 6th, 2021 (Parts Update, PCB Update)
### Parts Update
Since there are multiple parts that are unavailable for order, we decided to switch them in our design completely. The parts affected by this change are:
1. Voltage regulators (switch from LP590X series to LM1117 series as used in the soldering assignment)
2. Accelerometer (switch to ADXL343, which is bigger and easier to solder, while still keeping the digital interface)

### PCB Update
After changing the parts, we needed to draw the schematic again. We made new connection to the new accelerometer, added debouncing resistors for the buttons, and included new regulators to power the subsystems separately as usual. The following is the updated schematic.
![PCB2](./screenshots/PCB2.png)
As the TA suggested in the PCB review, we tried to not use the backside as much as possible, and kept each subsystem separate to have an easier time drawing the tracks. I then ordered the PCB on PCBWay with Stencils to aid in soldering. Here is the complete footprint:
![footprint2](./screenshots/footprint2.png)

## October 9th, 2021 (Parts Order, SPI Prototype)
### Parts Order
After submitting the PCB for manufacture, I completed ordering all the remaining parts on digikey. This includes the jtag connectors, crystals, and all the peripherals that are not super critical to our project. All parts should be ready for assemble in late October.

### SPI Prototype
Zhiyuan and I have been working on the development board with the accelerometer evaluation board, aiming to have a complete code base for SPI connection and interface with the accelerometer. We have figured out the way to configure all the SPI ports on an STM32 chip, with the correct data rate, clock rate, data size, etc. We then read the manual for HAL SPI functions for the MCU, as well as the SPI protocol for the accelerometer, in order to come up with the basic functions to write and read from the accelerometer:
```
void adxl_write (uint8_t address, uint8_t value)
{
	uint8_t data_send[2];
	data_send[0] = address|0x40;  // multibyte write
	data_send[1] = value;
	HAL_GPIO_WritePin (NSS2_GPIO_Port, NSS2_Pin, 0);  // pull the cs pin low
	HAL_SPI_Transmit (&hspi1, data_send, 2, 100);  // write data to register
	HAL_GPIO_WritePin (NSS2_GPIO_Port, NSS2_Pin, 1);  // pull the cs pin high
}

void adxl_read (uint8_t address)
{
	address |= 0x80;  // read operation
	address |= 0x40;  // multibyte read
	HAL_GPIO_WritePin (NSS2_GPIO_Port, NSS2_Pin, 0);  // pull the pin low
	HAL_SPI_Transmit (&hspi1, &address, 1, 100);  // send address
	HAL_SPI_Receive (&hspi1, data, 6, 100);  // receive 6 bytes data
	HAL_GPIO_WritePin (NSS2_GPIO_Port, NSS2_Pin, 1);  // pull the pin high
}

uint8_t adxl_read_byte (uint8_t address){
	uint8_t res;
	address |= 0x80;  // read operation
	address |= 0x40;  // multibyte read
	HAL_GPIO_WritePin (NSS2_GPIO_Port, NSS2_Pin, 0);  // pull the pin low
	HAL_SPI_Transmit (&hspi1, &address, 1, 100);  // send address
	HAL_SPI_Receive (&hspi1, &res, 1, 100);  // receive 6 bytes data
	HAL_GPIO_WritePin (NSS2_GPIO_Port, NSS2_Pin, 1);  // pull the pin high
	return res;
}
```
After this, we figured out all the registers we need to initialize for the sensor to behave like how we want it to be. This includes setting the sampling rate to 1600Hz, setting sensitivity to +-4g, and activating the tap functionality with the correct threshold and duration (this could achieve the function where we tap the sensor to re-calibrate the mouse)
```
void adxl_init (void)
{
	adxl_write (0x31, 0x01);  // data_format range= +- 4g
	adxl_write (0x2d, 0x00);  // reset all bits
	adxl_write (0x2d, 0x08);  // power_cntl measure and wake up 8hz

	adxl_write (0x2f, 0x00); // int1 for single tap.
	adxl_write (0x2e, 0x40); // interrupt enable single tap enabled

	adxl_write (0x1d, 0x20); // threshold_tap max 16g
	adxl_write (0x21, 0x30); // DUR currently 625 us
	adxl_write (0x22, 0x30); // wait time between taps
	adxl_write (0x23, 0x40); // wait time
	adxl_write (0x2a, 0x0E); // tap_axes suppress + xyz enabled.

	adxl_write (0x2c, 0x0e); // set date output data rate to 1600Hz
}
```
Polling was easy after the above code was established, for every MCU clock cycle, we would poll the data from the accelerometer, convert it to position difference between time intervals, and try to sense if there is an interrupt from tapping the sensors (we should potentially implement this as an actual interrupt and not some polling flag).
```
while (1)
{
  adxl_read(0x32);
  x = ((data[1]<<8)|data[0]);
  y = ((data[3]<<8)|data[2]);
  z = ((data[5]<<8)|data[4]);
  a1[0] = x*.0078;
  a1[1] = y*.0078;
  a1[2] = z*.0078;

  for(int i=0; i<3; i++){
    v1[i] = v0[i] + a0[i] + ((a1[i] - a0[i])/2);
    delx[i] = v0[i] + ((v1[i] - v0[i])/2);
    a0[i] = a1[i];
  }
  printf("\r\ndata: %.2f %.2f %.2f", delx[0], delx[1], delx[2]);
  uint8_t res = adxl_read_byte(0x30);
  int tap = HAL_GPIO_ReadPin(IN1_GPIO_Port, IN1_Pin);
  if(tap){
    printf("\r\ntap");
  }

}
```
Next step on our list is to refine the position calculation based on our actual use case. Zhiyuan suggest that we poll a couple acceleration samples and use the average to calculate a more stable position difference. I think it is certainly something to try, after we figure out a way to verify these calculated position.

## October 11th, 2021 (BT Module, Battery)
Today I received most of the parts. Since the PCB is still not here, Zhiyuan and I decided to just solder some wires out of RN42 for software implementation. So I connected Vdd, GND, and UART ports to some wires and then onto the breadboard. Zhiyuan got the battery and charger and we started to test the battery's functionality based on our verification table. We didn't record the specific numbers for initial battery verification. All we did was charging the battery until around 4.0 V, and tested that the battery can sustain 80 mA current for around 30 min. We plan on working on the BT configuration and UART connection tomorrow.


## October 18th, 2021 (BT Module + Accelerometer)
### BT Module Soldering and Testing
This past week we first soldered some pins out of the BT Module, in order to test it with the development board. We only soldered VDD, GND, RX, TX pins to test the bare minimum functionality of the device. Then we followed the RN42 data sheet to configure the module through BT. The process (described below) was fairly easy and we were surprised how quick we were able to get RN42 into HID mode.
1. Power on RN42 with 3.3V VDD
2. Connect the device to PC through as a BT device, just like how we connect any BT device in general
3. Within 60 seconds, open a serial monitor using the standard baud rates, data width, and COM port, and type "$$$" follow by return
4. "SH,0230" follow by return, to set RN42 into combo mode (which can take in all types of user inputs such as mouse, keyboard, volume control)
5. "S~,6" follow by return, to enable HID profile and disable all control/configuration normal usage upon reboot
6. "R,1" follow by return, to reboot the device
7. Follow step 2 to reconnect the device and the computer recognizes RN42 as a keyboard/mouse input device

While setting up was easy, we weren't able to send any HID raw reports to the BT module so that we move the cursor on the computer. We ordered a USB to UART/serial connector to connect to the development board. This way we can send UART signals from the board to a serial monitor on the PC. As it turns out, the UART pin on the development board is internally connected, so no signal would travel out to the world under normal condition and the data would travel back through the USB pin of the development board to print in that COM port. Once we use the other UART TX/RX pins on the development board, we could send valid mouse data to the PC to make cursor move or click.

### Accelerometer with BT
Since we already have the barebone code to translate accelerometer data into position difference, we decide to try using the accelerometer module to control the cursor. All we added in the original accelerometer code are a couple lines to transmit position to BT module using a UART port. The HID raw report (containing the mouse information is formatted as below)

![HID_format](./screenshots/hid_format.png)

```
uint8_t mouse[] = {0xFD, 0x05, 0x02, 0x00, delx[0], delx[1], 0x00};
HAL_UART_Transmit(&huart1,mouse,sizeof(mouse),10);
```
With the existing code, we were able to send extremely noisy data to the computer, as shown in the [video](https://gitlab.engr.illinois.edu/zy8/ece445-lab-notebook/-/blob/develop/notebooks/Jeff%20Chang/video/acce.MP4).

It is promising that we can send data to the computer, so all we need to worry now is the PCB and the algorithm to translate sensor data into correct HID raw report.

## October 24th, 2021 (BT Module + Gyroscope)
### Gyroscope
PCB as well as the stencils arrived this week. Since the MCU still hasn't arrived, we just soldered the gyroscope PCB using parts we have. Using the stencil makes soldering very easy. All we had to do was to apply solder paste through stencil, put on the part, and melt the solder paste with heat gun.

<p align="center">
  <img src="./screenshots/gyro.JPG" width="300" >
</p>

With the 7 pinouts from the PCB design, we could connect the gyroscope pcb to the development board and design the code for the gyroscope just like what we did for accelerometer. The general data retrieval was extremely similar to acceleromter. That is, we first need to figure out the SPI protocol our gyroscope uses, and write the associate write, read, and initialize functions. The following code closely resembles that of the accelerometer, but with different registers and different SPI format.

```
void gyro_write (uint8_t address, uint8_t value)
{
	uint8_t data_send[2];
	data_send[0] = address;
	data_send[1] = value;
	HAL_GPIO_WritePin (NSS_GPIO_Port, NSS_Pin, 0);  // pull the cs pin low
	HAL_SPI_Transmit (&hspi1, data_send, 2, 100);  // write data to register
	HAL_GPIO_WritePin (NSS_GPIO_Port, NSS_Pin, 1);  // pull the cs pin high
}

void gyro_read (uint8_t address)
{
	address |= 0x80;  // read operation
	HAL_GPIO_WritePin (NSS_GPIO_Port, NSS_Pin, 0);  // pull the pin low
	HAL_SPI_Transmit (&hspi1, &address, 1, 100);  // send address
	HAL_SPI_Receive (&hspi1, data, 4, 100);  // receive 4 bytes data
	HAL_GPIO_WritePin (NSS_GPIO_Port, NSS_Pin, 1);  // pull the pin high
}

void gyro_init (void)
{
	gyro_write(0x6b, 0x80);
	gyro_write(0x1b, 0x18);
}
```

In our use case, gyroscope data is very easy to manipulate to our desired output. All we had to do was poll sensor data, and manually set some positive and negative threshold to indicate a mouse click and a mouse release, as shown int the code below.

```
gyro_read(0x43);
x = ((data[0]<<8)|data[1]);
y = ((data[2]<<8)|data[3]);
if (x >= 11000 ){
  click_flag = 1;
}
if (x <= -10000 ){
  release_flag = 1;
}
if (click_flag){
  cx = snprintf(Test, 50, "\r\n \r\nclick!");
  HAL_UART_Transmit(&huart2,Test,cx,10);
  click_flag = 0;
}
if (release_flag){
  cx = snprintf(Test, 50, "\r\n\r\nrelease!");
  HAL_UART_Transmit(&huart2,Test,cx,10);
  release_flag = 0;
}
HAL_Delay(200);
```

We were able to sense clicks very easily with this code, but the MCU runs a little too fast so that when we take out the delay, we will sense a ton of clicks with only one click. We think we will need to add some click timer in the system to manually set time thresholds to reduce duplicate click sensing.

## October 31st, 2021 (Wait on parts, Gyroscope Timer)
### Wait on Parts
The MCU still needs time to come, so we still cannot assemble our first PCB at this point. We decided to wait for one more week, and if we can't we will just take off the MCU from the development board and assemble with that.

### Gyroscope Timer
The problem we had with the gyroscope algorithm was that every gyroscope movement will trigger a lot of clicks because the MCU CLK speed significantly exceeds our finger movement, so if each finger click is 0.2 seconds, we will be getting 0.2/clk seconds amount of clicks. We had 2 approaches to fix this problem
1. use a variable as counter and increment it when polling, only sense a click if counter reaches a particular value. We did not use this method because our while loop time can change very easily because we also have to accommodate accelerometer and other functionality, which can easily change computation time and mess up the gyroscope algorithm.
2. use a hardware timer to actually time the clicks with no interference of computation time. This is slightly harder to do because we have to set up timer using the system clock when configuring the MCU. We first enable initialize the hardware timer that counts up to 65535 in 60 seconds based on a 24MHz system clock.  
```
MX_TIM2_Init();
HAL_TIM_Base_Start_IT(&htim2);
```
Then with a variable that counts the roll over, we can calculate the absolute time disregard of the software computation time.
```
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    rollover_cnt++;
}
// then we can use __HAL_TIM_GET_COUNTER(&htim2) + rollover_cnt * 65535 for time
```

With this implementation, we modify our gyroscope code so that we only register a click if((see a click from the gyroscope)&&(the time between this click and the previous click is within a threshold)). We also force the first click to be registered by hard coding. The complete click code is as follows.
```
diff_time = (click_time == 0) ? 1100 : (__HAL_TIM_GET_COUNTER(&htim2) + rollover_cnt * 65535 - click_time);
if (click_flag && diff_time>300){
  cx = snprintf(Test, 50, "\r\n \r\nclick! %d", diff_time);
  HAL_UART_Transmit(&huart2,Test,cx,10);
  click_flag = 0;
  click_time = __HAL_TIM_GET_COUNTER(&htim2) + rollover_cnt * 65535;
}
```
The code is very similar for releasing a mouse click
```
r_diff_time = (release_time == 0) ? 1100 : (__HAL_TIM_GET_COUNTER(&htim2) + rollover_cnt * 65535 - release_time);
if (release_flag && r_diff_time>300){
  cx = snprintf(Test, 50, "\r\n\r\nrelease! %d", rollover_cnt);
  HAL_UART_Transmit(&huart2,Test,cx,10);
  release_flag = 0;
  release_time = __HAL_TIM_GET_COUNTER(&htim2) + rollover_cnt * 65535;
}
```

As a result, we were able to successfully program the MCU to detect clicks at a final demo accuracy. Here is a [video](https://gitlab.engr.illinois.edu/zy8/ece445-lab-notebook/-/blob/develop/notebooks/Jeff%20Chang/video/gyro.MP4) showing how well we were able to do this.



## November 7th, 2021 (debug PCB, new PCB, gyroscope)
### Debug PCB
We got scammed by the MCU distributor, so we decided to break apart our development boards and assemble the PCB. Upon completion of the PCB, we realized a couple issues.
1. We can't power up the board. By probing the PCB pads a little, we realized that VIN and GND are shorted. This is due to power line passing through some through holes that contains GND, which effectively short them. In the figure below, you can see VIN (green trace) connecting all gyroscope SPI and GND together into one giant net.

<p align="center">
  <img src="./screenshots/footprint3.JPG" width="300" >
</p>

Our temporary solution to this issue was a simple one. We simply manually cut the green trace on the PCB with a knife, separating the connected net into 7 independent pads, and gave up using the original Vin/GND connection. We soldered physical wires on one of the pads that connects VIN/GND to one of the voltage regulators. They are used for VIN and GND instead. This way, we can at least power the electronics.

2. We were then able to power on the BT module, as we could detect the BT input device on a PC. However, we weren't able to connect to the MCU through SWD (serial wire debug) and the MCU was insanely hot. First, we checked the current draw from the MCU and it was 700mA. It seemed logical that the MCU was acting like a low resistor just like a burnt chip. We then compared the impedance of a working chip on a development board (~60 kOhm) with that on the PCB (~6 Ohm) and found they were very different. We then looked into the maximum temperature the manufacturer suggested for soldering the MCU, and soldered a new MCU on the PCB, but the same thing happened again. We then realized the issue was that we soldered the MCU in the wrong orientation (there were two diagrams on the data sheet with different orientations). After resoldering, we were able to recognize the MCU and program the chip successfully.

3. We realized when creating gerber files, kicad would reassign some traces based on our design, and that disconnected a couple ground pins on our accelerometer (you can see this in the final kicad layout above). We did not notive that and end up not connecting the accelerometer to the PCB. We will not be able to fix this issue temporatily so we decided to first test on gyroscope implementation and fix the accelerometer in the next PCB design.

### New PCB Design
1. PCB ver 1.1
Since we verified that the original PCB works with the power shorting issue resolved, we were confident that if we simply connect the GND pins of the accelerometer and reroute the Vin power trace, we would have a functional PCB. This way, we won't even need to reroute everything or buy a new stencil.
![footprint4](./screenshots/footprint4.png)

2. PCB ver 2.0
We realized that the PCB is a bit large to place on a hand, so we decided to remove all unnecessary components and create a new layout with smaller footprint and trace sizes. Specifically, we took out oscillator (we will just use the internal 24Mhz clock), USB (because SWD programming works), wake up button (replaced with reset button). We also changed all gyroscope connectors to 1.27mm through holes just like the SWD connector, in order to reduce space. We also changed the voltage regulators for 3.3 V and 2.5 V because the original voltage regulators proved to have too high of a dropout voltage (1.2 V at times). In order to have a more stable voltage input for BT/MCU/sensors, we decided to use a better LDO voltage regulator TC1264 (nominal 0.020 V at 100uA current draw). The resulting layout are shown below.
![footprint5](./screenshots/footprint5.png)
![footprint6](./screenshots/footprint6.png)

### Gyroscope + PCB
In terms of programming the MCU to work with gyroscope on the PCB, it was almost exactly the same as on the development board. So as we fixed the powering issue, we were able to move the gyroscope code onto the PCB with minimal work.

## November 14th, 2021 (Wait on PCB, accelerometer)
I didn't do much this week. Zhiyuan said he is going to look at the accelerometer algorithm, so I gave him the module and just relaxed this week. By the end of the week, he was able to get the accelerometer to move the mouse based on rotation, which is different from translational movement as we thought it was going to be initially. However, if we can't compute the translational movement well enough, we might end up using rotation as input to control the mouse cursor.

## November 21st, 2021 (PCB Ver 2.0, Accelerometer, BT Latency)
### PCB Ver 2.0
PCB came this week and we immediately soldered it and tested the ver 2.0 PCB. With minimal re-soldering, we were able to test that both PCBs function as intended, capable of sending both accelerometer and gyroscope data to the PC via BT. In short, all we need to do at this point is to finish up the software algorithm implementation and refine accuracy and stability for the final demo.
### Accelerometer Algorithm Modification
This weekend we focused on refining the accelerometer algorithm. The previous algorithm wasn't able to translate acceleration data into meaningful cursor movement. However, we made a couple changes that removed some issues, and was able to move the cursor horizontally when we move the PCB sideways (with some flickering and noise issues of course). Here is the [video](https://gitlab.engr.illinois.edu/zy8/ece445-lab-notebook/-/blob/develop/notebooks/Jeff%20Chang/video/acce2.MOV).

1. Averaging acceleration data to reduce noise. Instead of reading each acceleration for calculation, we read the value into a "temp1" variable, and taking the average of 128 of those temp1 values. With this method, we could reduce the effect of some outliers in our samples.
```
temp1[0] = ((data[1]<<8)|data[0]);
temp1[1] = ((data[3]<<8)|data[2]);
temp1[2] = ((data[5]<<8)|data[4]);
a1[0] += temp1[0];
a1[1] += temp1[1];
a1[2] += temp1[2];
if(a_avg_count == 127){
    for(int i=0; i<3; i++){
      a1[i] = a1[i] / 128 - offsets[i]; //offset is the calibration offset
    }
}
```

2. We have a significant bug in our code that the cursor would move in the opposite direction at the end of each movement. This results from velocity calculated from acceleration integration not being zero. We expanded our velocity zeroing algorithm by counting the number of 0 acceleration at the end of each movement. In our current implementation, whenever there are 30 consecutive 0 acceleration, we recognize this event as an end of a hand movement, and set velocity to be 0, so the cursor doesn't continue moving in any unexpected paths.
```
for (int i = 0; i < 3; i++){
    if (abs(temp1[i]) < temp_threshold){
      temp1[i] = 0;
    }
    if(temp1[i]==0){v_zero_count[i]++;}
    else{v_zero_count[i]=0;}
    if(v_zero_count[i]>=v_zero_threshold){
      v1[i] = 0;
      v0[i] = 0;
    }
}
```
But just as the code before, we still have the original velocity threshold when "integrating" the acceleration
```
if (abs(v1[i]) < 50){
    v1[i] = 0;
}
```

### BT Latency
In our high level requirements, we wrote that we need our mouse to have a maximum of 8ms latency (corresponding to a 125Hz refresh rate). While we can't necessarily measure the latency from the physical movement to the cursor movement, we were going to verify that the mouse events a PC registers has a latency less than 8ms. Today, we use the [applet](https://www.vsynctester.com/testing/mouse.html) to test the mouse latency. We found that with our current configuration, we were only able to achieve 15ms latency. We then started looking at ways to increase throughput or decrease latency of the RN42 module.
1. (SU,92) This BT command increase the baudrate of UART connection to 921k from 152k. Baud rate should not have been an important factor in increasing latency because it is the connection between MCU and RN42 and not RN42 and the PC. But we still increased it hoping for any data transfer speed increase from baud rate.
2. (SI,0200; SJ,0200) By setting the inquiry and scan window to 200 (more than double from the original 0060), we could decrease the usage of power to constantly try to make it self discoverable or connectable. We then indirectly give RN42 more power to use for data transfer.
3. (SQ,16) This command optimizes RN42 for latency rather than throughput. High throughput could also mean high latency if all data packets are sent in a short time interval inconsistently.

With all the above configuration, we were able to decrease the latency to around 6ms, which is way within our desired requirements. At this point, we already have promising results for 2 of our high level requirements (mouse functionality and low latency). We will focus our energy refining the accelerometer algorithm in the following days.

## November 26th, 2021 (Accelerometer, Basic Functionality, Advanced Functionality)
### Accelerometer
We updated our algorithm to detect the end of a cursor movement. We previously had a counter that counts 30 consecutive 0 acceleration, but this introduced 2 problems. First, as we lower the acceleration threshold from 20 to 5, to accommodate for slower cursor movements, it became likely that acceleration would have noise that surpass 5. This resulted in occasional fail to zero out velocity, resulting in the cursor moving in the opposite direction after the end of the motion (as the velocity that doesn't get zeroes out is the result of deceleration). Second, with stable hands, it is sometimes possible to move in constant velocity, allowing the acceleration to move to zero during the motion. This would zero out the velocity, and result in very large cursor irregularity.

To solve the problem, I proposed to rewrite the condition for zeroing velocity. Here v1 represents velocity of the current t, v0 represent velocity at t-1, v00 represent velocity at t-2. While this can't fix the first issue entirely, we are not so dependent on acceleration being below 5 anymore because we have a threshold for velocity = 40. So the probability of not zeroing velocity when acceleration is low is significantly reduced. As of the second issue, a constant velocity would not affect the algorithm any more because the condition doesn't include it.
```
if(v1[i]!=0 && v1[i]==v0[i] && v0[i] == v00[i]){
 v1[i] = 0;
}
```

After this, we extended the accelerometer to also handle acceleration in the z axis. We have done our code in a for loop that calculated all 3 axes, so the functionality extension is merely adding a term in the HID report.
```
mouse[4] = -1*(delx[1]>>5); //y axis of mouse (x axis of cursor)
mouse[5] = -1*(delx[2]>>6); //z axis of mouse (y axis of cursor)
```

### Basic Functionality
As our code for gyroscope transferred over very well, we was able to write the functions into compact function calls (for details of the functions look in gyroscope implementation above).
```
gyro_read_all(gyro_x, gyro_y);
gyro_clickop(1, gyro_x, c_diff_time, r_diff_time, click_time, release_time, click_flag, release_flag, active_flag);
gyro_clickop(2, gyro_x, c_diff_time, r_diff_time, click_time, release_time, click_flag, release_flag, active_flag);
gyro_clickop(3, gyro_x, c_diff_time, r_diff_time, click_time, release_time, click_flag, release_flag, active_flag);
```
With just 4 lines, we use the previous algorithm to note if a gyroscope just clicked or has released the button using the variable active_flag[i]. When active_flag==1, it means the gyroscope has just clicked. When active_flag!=1, it means the button has been released. Using the different combinations of these clicks, we wrote the code for the basic functionalities.
#### 1. Left/Right Click
We design the system to use index finger (2nd gyroscope) and middle finger (3rd gyroscope) as left click and right click respectively, when the thumb is not bent (1st gyroscope). The thumb is basically an indicator of whether we are in normal cursor mode (not bent), or in special operation mode (bent). This translates to first looking at the condition of the thumb, then alter the click bit for with left or right click in the HID report.
```
if(active_flag[0] == 1){...}
else {
  if(active_flag[1] == 1){
    mouse[3] |= 0x01;
  }
  else if(active_flag[1] == 2){
    mouse[3] &= 0xFE;
  }
  else if(active_flag[2] == 1){
    mouse[3] |= 0x02;
  }
  else if (active_flag[2] == 2){
    mouse[3] &= 0xFD;
  }
}
```
#### 2. Scroll
As described above, we use thumb to indicate whether the device is in special operation mode. Scrolling has been designed to be part of the special operation mode. The user must first bend the thumb, then use their index finger to scroll the page (or they can use all other fingers together but only the data from index finger is used for calculation). Specifically, we would continue to send scroll HID reports (slowed by the scroll_count or else it scrolls too much), until the finger moves in the opposite direction or until the thumb releases (thumb releases is more user friendly). The same code is applied to both scroll up and down
```
if(active_flag[1] == 1){
  mouse[6] = -1;
  if(scroll_count==15){
    HAL_UART_Transmit(&huart2,mouse,sizeof(mouse),10);
    scroll_count = 0;
  }
  scroll_count++;
  mouse[6] = 0;
  if(gyro_x[1]>5000){
    active_flag[1] = 0;
  }
}
else if(active_flag[1] == 2){
  mouse[6] = 1;
  if(scroll_count==15){
    HAL_UART_Transmit(&huart2,mouse,sizeof(mouse),10);
    scroll_count = 0;
  }
  scroll_count++;
  mouse[6] = 0;
  if(gyro_x[1]<-5000){
    active_flag[1] = 0;
  }
}
```
### Advanced Functionality
#### 1. Zoom in/out
This function falls under the special operation category. After clicking the thumb, when the user tilt the glove in the x direction (along middle finger) until a certain angle (approximately 35 degree), the code would continuously send ctrl+"+" key presses (again with speed controlled using a counter). This is calculated using the acceleration data.
```
if(a1[0]<=-100){
  if(zoom_in_count==30){
    HAL_UART_Transmit(&huart2,zoom_in,sizeof(zoom_in),10);
    HAL_UART_Transmit(&huart2,ctrl_release,sizeof(ctrl_release),10);
    zoom_in_count = 0;
  }
  zoom_in_count++;
} else if(a1[0]>=100){
  if(zoom_out_count==30){
    HAL_UART_Transmit(&huart2,zoom_out,sizeof(zoom_out),10);
    HAL_UART_Transmit(&huart2,ctrl_release,sizeof(ctrl_release),10);
    zoom_out_count = 0;
  }
  zoom_out_count++;
}
```
#### 2. Volume up/down
Similar story applies to volume control. Under special operation mode, as we tilt the glove in the y axis (along the watch direction), the acceleration in this axis would increase due to gravitational force, and we use this in a condition for turning up or down volume. Here we have the degree set to around 63 degrees but this is subject to change.
```
if(a1[1]<=-180){
  if(vol_down_count==10){
    HAL_UART_Transmit(&huart2,vol_down,sizeof(vol_down),10);
    vol_down_count = 0;
  }
  vol_down_count++;

} else if(a1[1]>=180){
  if(vol_up_count==10){
    HAL_UART_Transmit(&huart2,vol_up,sizeof(vol_up),10);
    vol_up_count = 0;
  }
  vol_up_count++;
}
```
#### 3. Calibration
In our testing of the accelerometer, we realized it is extremely hard for us to keep the hand steady as so the axis doesn't get affected by gravitational force. In future design, a gyroscope along with the accelerometer is probably a better design as we can used the angle information to calculate affect this might bring to acceleration. Anyway, if the gravitational offset gets out of hand and starts moving the cursor around randomly, we need some quick way to calibrate the cursor again. Since the x-axis of the accelerometer is not used in normal mode, we decide to use this for calibration. If we lift our hand so the middle finger is pointing up, we would see a huge increase of acceleration on the x-axis due to gravity. We then use this as a condition to first pause the MCU for 1 second (allowing the user to stabilize their hand), then recalibrate the system. The calibration function simply zeroes out every critical value, average 100 acceleration data points to be the new acceleration offset.
```
if(a1[0]>=220){
  cx = snprintf(Test, 50, "\r\ncalibration");
  HAL_UART_Transmit(&huart1,Test,cx,10);
  HAL_Delay(1000);
  calibration(a0, a1, v0, v00, offsets, v_zero_count, 100);
  HAL_Delay(1000);
  continue;
}
```

## Dec 2nd, 2021 (Acceleration, Backup Code, Verification)
### Acceleration
We added a final section to strengthen the end-of-movement detection algorithm. This code basically book keeps which direction we are going, and doesn't allow velocity to go in the opposite direction in a certain period of time. Specifically, we use old_mouseX and old_mouseY to store the previous direction, and set the mouse direction to be zero when we see a jump of sign.
```
if ((mouse[4] > 0 && old_mouseX < 0) || (mouse[4] < 0 && old_mouseX > 0)) {
  mouse[4] = 0;
}
else {
  old_mouseX = mouse[4];
}
if ((mouse[5] > 0 && old_mouseY < 0) || (mouse[5] < 0 && old_mouseY > 0)) {
    mouse[5] = 0;
}
else {
  old_mouseY = mouse[5];
}
```
### Backup Code
In our testing, we realized it might be more stable to use components of gravitational acceleration for calculating position update. If we tilt our hand to the right, we will see the gravitational acceleration component in the y direction. Using these values, we can simply add a counter and continuously send position update in that position just like how we wrote the code for volume control. In the code below, you can see we set a threshold to see if the hand is tilted enough, then just continuously send a +5 or -5 position update to the PC using mouse[4] and mouse[5], until that tilt in the device goes below 50. Since our usual implementation is prone to hand instability, we plan on flashing this version during demo as well.
```
if(abs(a1[1])>=50) {
  mouse[4] = 5*(a1[1]/abs(a1[1]));
}
else {
  mouse[4] = 0;
}
if(abs(a1[0])>=50) {
    mouse[5] = -5*(a1[0]/abs(a1[0]));
}
else {
    mouse[5] = 0;
}
```
### Verification
After all the code implementation, I conducted the verification for most subsystems. All the data are collected in the file "data445.xlsx" and will be presented in the presentation, so I will not copy them here again. I took videos of all the testing and that information has been pushed under "[demo/demo_videos](https://gitlab.engr.illinois.edu/zy8/ece445-lab-notebook/-/tree/develop/demo/demo_video)"

# This marks the end of the project.


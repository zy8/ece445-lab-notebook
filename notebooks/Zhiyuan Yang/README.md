# ECE445 Zhiyuan Yang (zy8) Notebook
[[_TOC_]]
# 2021-09-20 - Parts Selection:
Today I looked at two sensors.
First, I was looking at the accelerometer sensor selection.
adxl335 is a three-axis analog accelerometer. Power supply needs decoupling. Sensitivity is about 0.3 v /g. Power: 9uW at 3V. Good but I would prefer digital sensors. I would look into the difference between the digital sensors and analog sensors.

![adxl335](./Pictures/adxl335.PNG)

Then I looked at the gyroscopes sensors and came up with LY3200ALH.
It is a high performance ±2000 dps analog yaw-rate gyroscope.
![LY3200ALH](./Pictures/LYH.PNG)

However, since it is a analog sensor, we need to implement some high or low pass filters. I am not sure about this circuit filtering thing. I took a screenshot of a recommended circuit design of the LY3200ALH. If we are going all analog sensors, we probably need to do a lot to reduce noise in the circuit.
# 2021-09-22 - Parts Selection Continued:
Today I looked at more parts that our device could use.

The digital sensors are preferred over analog counter parts. I found out two reasons. First, there will be less noise on the output signal. Second, the interface with the MCU will be easier. We decided to go with a STM32 L1 chip, which has
• 11x peripheral communication interfaces
– 1x USB 2.0 (internal 48 MHz PLL)
– 5x USARTs
– Up to 8x SPIs (2x I2S, 3x 16 Mbit/s)
– 2x I2Cs (SMBus/PMBus) (This was found on the MCU datasheet). I previously thought that digital sensors would be much more power intensive than analog sensors.
However, because most of the digital sensors are made more recently (usually after 2016). They are actually more capable and saves more power as well. I have decided to go all digital sensors.

# 2021-09-24 - STMIDE programing & acceleration paper.
Jeff introduced something about the IDE programing that we are gonna use. I installed all the software concerning stm32 development. I followed the tutorial(https://wiki.st.com/stm32mcu/wiki/Category:STM32_step_by_step) And I installed CubeIDE, STM32CubeMX, STM32CubeProg,STM32CubeL4 Firmware.

Cube IDE is for developement of project. MX is a graphical interface for assigning GPIO pins on the MCU. STM32CubeProg is for check the programmer like st-link.

Jeff handed me the development board to play with and he taught me how to run a simple tutorial described below.

Tutorial: I followed the Blink LED tutorial and created the first example on the development board. I first marked the GPIO pins and configurations on STM32CubeMX. Then I wrote a simple while loop to toggle the LED GPIO pin.

I found this acceleration calculatio paper online. Here is a copy of the bibtex citation that we could use in our paper.
```
@article{seifert2007implementing,
  title={Implementing positioning algorithms using accelerometers},
  author={Seifert, Kurt and Camacho, Oscar},
  journal={Freescale Semiconductor},
  volume={1},
  pages={13},
  year={2007}
}
```
![LY3200ALH](./Pictures/acc_paper.PNG)
![LY3200ALH](./Pictures/acc_algo.PNG)

It showed us how to simplified the double integration process without huge errors on the final calculations. Another neat thing about this is that all the calculations will be using additions and left shifts which are simple operations which would not stress our MCU by too much.
# 2021-09-30 Parts selection complete and finished Design Doc
After much more consideration, I decided on two digital sensors that would do the job.
![acce_d](./Pictures/acce_digital.png)
![gyro_d](./Pictures/gyro_digital.png)

I will be using LIS2DE12 and ICG-1020S for the project. The reason for my selection is that they both use SPI connection. They have selectable ranges. For accelerometer it is best if we can use the range of +/- 4g since it is most suitable for hand movements. If range is too big, then we are sacrificing precision. That is why I choose the ICG-1020S to be the gyroscope, it has range of 374 degrees/sec and a output word length of 16bit. So this device can be really accurate. The only downside is that LIS2DE12 has only 8 bit output word length, which means it is not very accurate. The following is the complete schematics design that i did with Jeff.
![PCB1](./Pictures/PCB1.png)

I chose the LP5900 voltage regulators because they are very new and super low dropout and super good performance. They are nearly out of stock so i got the packaging of it (NOPB) that is smaller than SOC-223.
# 2021-10-06 PCB update and ordered

![PCB2](./Pictures/PCB2.png)

Jeff did most of the footprint of our PCB design. Then he found out 2 problems. First the packaging of the voltage regulators are too small so he changed the regulators to the ones we used in the soldering assignment. Second, I noticed that the accelerometer that we were gonna use was out of stock so we switched to adxl343. I checked its datasheet and I found out that it is actually a better option than the original one because its output word length is 13 bit which means that it is far more accurate. I helped Jeff with the schematics design part (redesigning the accelerometer and regulators). We sent out our first version of PCB order.
# 2021-10-09 accelerometer dev board implementation:

We got the MCU dev board and also the accelerometer adxl343 dev board. So we get to work on the accelerometer spi configuration.
![PCB2](./Pictures/PXL_20211014_030755927.jpg)
Here is a picture of the setup of circuits that we have.
The following is the steps we took to set up the spi connection.
1. configure the MCU's spi connection using STM32CubeMX (hspi1)
2. make sure that the timing requirements matches with the sensor(clock polarity clock phase)
3. figure out how spi works: if master wants to talk to slave, it pulls the cs pin low and write to it or read it. CS is pulled high otherwise.
4. developed the code below(basically read through the datasheet, see which registers to initialize and which regs to read).

```
void adxl_write (uint8_t address, uint8_t value)
{
	uint8_t data_send[2];
	data_send[0] = address|0x40;  // multibyte write
	data_send[1] = value;
	HAL_GPIO_WritePin (NSS2_GPIO_Port, NSS2_Pin, 0);  // pull the cs pin low
	HAL_SPI_Transmit (&hspi1, data_send, 2, 100);  // write data to register
	HAL_GPIO_WritePin (NSS2_GPIO_Port, NSS2_Pin, 1);  // pull the cs pin high
}

void adxl_read (uint8_t address)
{
	address |= 0x80;  // read operation
	address |= 0x40;  // multibyte read
	HAL_GPIO_WritePin (NSS2_GPIO_Port, NSS2_Pin, 0);  // pull the pin low
	HAL_SPI_Transmit (&hspi1, &address, 1, 100);  // send address
	HAL_SPI_Receive (&hspi1, data, 6, 100);  // receive 6 bytes data
	HAL_GPIO_WritePin (NSS2_GPIO_Port, NSS2_Pin, 1);  // pull the pin high
}

uint8_t adxl_read_byte (uint8_t address){
	uint8_t res;
	address |= 0x80;  // read operation
	address |= 0x40;  // multibyte read
	HAL_GPIO_WritePin (NSS2_GPIO_Port, NSS2_Pin, 0);  // pull the pin low
	HAL_SPI_Transmit (&hspi1, &address, 1, 100);  // send address
	HAL_SPI_Receive (&hspi1, &res, 1, 100);  // receive 6 bytes data
	HAL_GPIO_WritePin (NSS2_GPIO_Port, NSS2_Pin, 1);  // pull the pin high
	return res;
}
```
```
void adxl_init (void)
{
	adxl_write (0x31, 0x01);  // data_format range= +- 4g
	adxl_write (0x2d, 0x00);  // reset all bits
	adxl_write (0x2d, 0x08);  // power_cntl measure and wake up 8hz

	adxl_write (0x2f, 0x00); // int1 for single tap.
	adxl_write (0x2e, 0x40); // interrupt enable single tap enabled

	adxl_write (0x1d, 0x20); // threshold_tap max 16g
	adxl_write (0x21, 0x30); // DUR currently 625 us
	adxl_write (0x22, 0x30); // wait time between taps
	adxl_write (0x23, 0x40); // wait time
	adxl_write (0x2a, 0x0E); // tap_axes suppress + xyz enabled.

	adxl_write (0x2c, 0x0e); // set date output data rate to 1600Hz
}
```
# 2021-10-15 BT module testing & acceleration algo
Jeff and I read through the paper and wrote part of the acceleration algorithm code
```
while (1)
{
  adxl_read(0x32);
  x = ((data[1]<<8)|data[0]);
  y = ((data[3]<<8)|data[2]);
  z = ((data[5]<<8)|data[4]);
  a1[0] = x*.0078;
  a1[1] = y*.0078;
  a1[2] = z*.0078;

  for(int i=0; i<3; i++){
    v1[i] = v0[i] + a0[i] + ((a1[i] - a0[i])/2);
    delx[i] = v0[i] + ((v1[i] - v0[i])/2);
    a0[i] = a1[i];
  }
  printf("\r\ndata: %.2f %.2f %.2f", delx[0], delx[1], delx[2]);
  uint8_t res = adxl_read_byte(0x30);
  int tap = HAL_GPIO_ReadPin(IN1_GPIO_Port, IN1_Pin);
  if(tap){
    printf("\r\ntap");
  }

}
```
So we are basically polling the data every while(1) cycle in the MCU.
I had an disagreement with Jeff because we were using double type numbers in our algorithm and that would cause significant delay in MCU calculation since floating calculations are bad.
Yayati and Jeff worked on the BT setup part. Basically, the bluetooth chip is powered on and connect to the MCU with UART. We first configure the BT chip wirelessly by connecting our PC with it and initialize it. They were successful in initialization and changed the module to HID mode. In HID mode, the bluetooth chip will act like a data pipe that transfers data it receives from the MCU to the PC that it is connected to.
```
uint8_t mouse[] = {0xFD, 0x05, 0x02, 0x00, delx[0], delx[1], 0x00};
HAL_UART_Transmit(&huart1,mouse,sizeof(mouse),10);
```
However, when we tested it, nothing showed up. I was largely involved with debugging. I created a helper function that can help with testing all the strings that we are sending with UART.
```
int16_t cx;
uint8_t Test[50];
cx = snprintf(Test, 50, "\r\nsize of data:%d %d %d", delx[1], v1[1], a1[1]);
HAL_UART_Transmit(&huart2,Test,cx,10);
```
uart2 is a com port that we can connect with the PC and this way we can write data out to PC to see it. We later found out that the problem is that one of the UART (uart2) is specifically used for USB data transmission on the dev board so when we want to use that uart port to communicate with the BT chip, we failed. Then, we verified that our BT chip is working.
# 2021-10-24 Gyroscope algorithm implementation
Today we configured the gyro_scopes in similar fashion as accelerometer. We did not get an dev board so Jeff soldered our pcb board for gyroscopes as a dev board. What we did is similar to that of acceleromter. We implemented the read and write functions. We initialized the device by writing to specific registers. We learned which register to write and read by studying the registers map section of ICG-1020S datasheet.
```
void gyro_write (uint8_t address, uint8_t value)
{
	uint8_t data_send[2];
	data_send[0] = address;
	data_send[1] = value;
	HAL_GPIO_WritePin (NSS_GPIO_Port, NSS_Pin, 0);  // pull the cs pin low
	HAL_SPI_Transmit (&hspi1, data_send, 2, 100);  // write data to register
	HAL_GPIO_WritePin (NSS_GPIO_Port, NSS_Pin, 1);  // pull the cs pin high
}

void gyro_read (uint8_t address)
{
	address |= 0x80;  // read operation
	HAL_GPIO_WritePin (NSS_GPIO_Port, NSS_Pin, 0);  // pull the pin low
	HAL_SPI_Transmit (&hspi1, &address, 1, 100);  // send address
	HAL_SPI_Receive (&hspi1, data, 4, 100);  // receive 4 bytes data
	HAL_GPIO_WritePin (NSS_GPIO_Port, NSS_Pin, 1);  // pull the pin high
}

void gyro_init (void)
{
	gyro_write(0x6b, 0x80);
	gyro_write(0x1b, 0x18);
}
```
# 2021-10-31 Gyroscope algorithm update
This week we did not do much, Jeff and I finished the gyroscope algorithm. Since clicking and releasing is just binary motion, we decided to just set up a threshold that if the velocity read exceeds it, a click motion will be registered. However, when we were testing, we found out that since our devices are so fast(gyroscopes 3khz and MCU 24mhz), multiple clicking motions will be registered during the click event. So what we decided to do is set up a timeout on the click and release events. no two consecutive clicks can happen in 200ms. This turns out to work pretty well. The following is the main part of our code.
```
MX_TIM2_Init();
HAL_TIM_Base_Start_IT(&htim2);
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    rollover_cnt++;
}
...
diff_time = (click_time == 0) ? 1100 : (__HAL_TIM_GET_COUNTER(&htim2) + rollover_cnt * 65535 - click_time);
if (click_flag && diff_time>200){
  cx = snprintf(Test, 50, "\r\n \r\nclick! %d", diff_time);
  HAL_UART_Transmit(&huart2,Test,cx,10);
  click_flag = 0;
  click_time = __HAL_TIM_GET_COUNTER(&htim2) + rollover_cnt * 65535;
}
```
We initialized a counter that counts up every 1 ms (htim2) by configuring the MCU using CubeMX. I think the timer utilized the system clock to count. However, the counter is 16-bit so it can only count up to 65K before rolling over. When it rolls over, we set up the MCU to send an interrupt. We set up a rollover counter to count up when it rolls over. So the counter is actually able to measure 2^32 minutes of time which is more than enough for us. We take the diff time between each clicking motions and only register it when they are 200 ms apart.
# 2021-11-2  PCB version 1 failed and debug
Today we had an emergency.

It turns out that our PCB board is shorted between VDD and GND and nothing can be powered when we turned it on.
![PCB2](./Pictures/PXL_20211103_055900681.jpg)
We figured out the reason is because the connector holes penetrated the main power line, which is VDD. One of the connector holes is GND.  
![PCB2](./Pictures/footprint3.JPG)
We are actively working to fix it and get on with testing. Hope for the best!
# 2021-11-6 PCB version 1 debugged and testing
Today we finished testing the MCU chip on our own PCB board and it works!

We deal with the shorting problem by cut out the VDD power line that connects to the GND connector hole, and power the circuit with the VDD and GND pins on the 3.3V voltage regulators instead, which connects to the rest of the circuit.

We soldered on the 64-pin MCU on the board. The process is extremely hard. First, we got scammed by our MCU distributor, who did not send the MCU to us. We had to buy the development board for STM32L152re and take the chip off to solder it on the board. Jeff did most of the soldering process and I helped him solder and test the part. We use heat gun to take off the part and stencil to solder the MCU on the PCB board. We checked on the Microscope to see if all the pins are connected to the board and add solder. When we finally soldered it on and powered up the chip, it was draining 700mA of current which is insane. The impedance of the chip is only 6 ohms but it was in kohms for the chip on the developement board. After exhausting all possibilities, we found out that we soldered on the chip with the wrong orientation. Finally, we got the second board working and successful programmed the chip.

We then confirmed that the shorting is the only issue with our PCB, and Jeff and I went on to design the second verion of PCBs.
![PCB2](./Pictures/PXL_20211106_040335598.jpg)
# 2021-11-8 PCB version 2&3 order
Today, we sent out our second and third version of the PCBs. The footprints are below.
![PCB2](./Pictures/footprint4.png)
![PCB2](./Pictures/footprint5.png)
![PCB2](./Pictures/footprint6.png)  
The second version is just the one that changed to powerline. Moved it out of the connectors. The third version, we made the PCB more compact to fit on the glove. We found out that the max clock speed by the chip alone is 24Mhz, and we originally had an oscillator on board to boost it up to 32Mhz. However, we figured that 24Mhz would be enough for our project to work so we took out the oscillator.

Jeff observed that our connectors for Gyroscopes board is too big and we could use the same connector as the one with the swd/jtag programmer. So we change them all to 1.27mm swd connectors. Jeff also took out the USB connector since we will only use swd connector. He also took out the wake up button and we could implement software sleep instead. I changed the 2.5V and 3.3V voltage regulators to a newer one TC1264 which is the same packaging but has lower dropout voltage. The old one has 1.2V dropout voltage, and BT module cannot work normally when we were using 3.7V battery.
# 2021-11-14 accelerometer algorithm update
When we were waiting for the new PCBs to arrive, we worked on the accelerometer algorithm which is the most important part of the project. To make the accelerometer value more precise. We averaged out 128 acceleration values. We also implemented a calibration procedure and we can take off offsets from the acceleration values.
```
temp1[0] = ((data[1]<<8)|data[0]);
temp1[1] = ((data[3]<<8)|data[2]);
temp1[2] = ((data[5]<<8)|data[4]);
a1[0] += temp1[0];
a1[1] += temp1[1];
a1[2] += temp1[2];
if(a_avg_count == 127){
    for(int i=0; i<3; i++){
      a1[i] = a1[i] / 128 - offsets[i]; //offset is the calibration offset
    }
}
```

```
for (int i = 0; i < 3; i++){
    if (abs(temp1[i]) < temp_threshold){
      temp1[i] = 0;
    }
    if(temp1[i]==0){v_zero_count[i]++;}
    else{v_zero_count[i]=0;}
    if(v_zero_count[i]>=v_zero_threshold){
      v1[i] = 0;
      v0[i] = 0;
    }
}
```
```
if (abs(v1[i]) < 50){
    v1[i] = 0;
}
```
Since we are integrating accelerations, our velocity values may not always end up zeroing. We have two solutions as described above.
1. When we counted enough zeros from acceleration values, we zero out the velocity.
2. When velocity is pretty low in a threshold, we zero it.

# 2021-11-16 bluetooth latency error solved
Today, we solved a bluetooth latency issue.

We changed to acceleration algorithm to average every 256 values. When we test it on the latency test we found online(https://www.vsynctester.com/testing/mouse.html). We are getting around 20ms of latency, which is lower than our high level requirement. However, when we changed it to 128, we are getting around 15ms of latency, which is better. But when we changed it to 64, the MCU is sending too many reports and overwhelms the bluetooth module. We tried all sorts of things. Read through the datasheet to find that  (SQ,16)  is a command we can use to optimize latency of our chip.

We then changed to a different browser(safari) since chrome might compress several mouse events into 1. We got 4-7 ms of latencies which are very good.
# 2021-11-21 PCBv3 assembly
Our new PCBs finally arrived!
We first performed some connection testing on the board to see if pins should be connected or disconnected. I was in charge of soldering the new gyroscopes boards.
![PCB2](./Pictures/PXL_20211124_032255655.jpg)
![PCB2](./Pictures/PXL_20211124_042510222.jpg)
They are small even with stencil. After I applied the solder paste using a stencil, I checked under the microscope to check if the paste is properly applied. Then I placed the gyroscope on the pads and use heat gun to solder. Finally, I checked under the microscope to see if all the connections to the pads are made. After soldering, I checked the impedance between the pins to see if the electrically properties are correct.
# 2021-11-26 All functionalities implemented and accelerometer algo update
This thanksgiving week we all got together and worked on the final functionalities for the project. The code for the project is in code section of the git repo and it is under PCB3_TEST2. We implemented a calibration function.
```
if(a1[0]>=220){
  cx = snprintf(Test, 50, "\r\ncalibration");
  HAL_UART_Transmit(&huart1,Test,cx,10);
  HAL_Delay(1000);
  calibration(a0, a1, v0, v00, offsets, v_zero_count, 100);
  HAL_Delay(1000);
  continue;
}
```
There is always a gravity component on the accelerometer. When it is flat on the table, it should read (x,y,z) = (0g,0g,1g). However, if our hand is raised up, the accelerometer is gonna be vertical to the ground at x axis and it will read (1g,0g,0g). In our accelerometer reading of 256 is 1g, so in the calibration code if x acceleration value (a1[0]) is above 220, meaning the hand is raised, then we perform the calibration function.

After discussion, we decided to use the gyroscope on the thumb to act like a basic/Advanced functionalities switch.

if(active_flag[0] == 1) means there is a click on the thumb gyroscope, we perform the basic functionalities such as cursor movement, and clicking.

if(active_flag[0] == 0) means we are in advanced mode. The gyroscopes click motion would translate to scroll down and release motion would translate to scroll up. When we tilt the accelerometer forward and backward, it would give negative and positive values on x-axis, which will be used for zooming-in/out. When the board is tilted left/right, it would give negative and positive values on the y-axis, which will be used for changing the volume up/down.

We also figured out all the hid reports for all the operations. We looked at the Roving Networks HID section a lot and this website is also very helpful.
https://wiki.osdev.org/USB_Human_Interface_Devices
```
uint8_t vol_up[] = {0xFD, 0x03, 0x03, 0x10, 0x00};
uint8_t vol_down[] = {0xFD, 0x03, 0x03, 0x20, 0x00};
uint8_t vol_release[] = {0xFD, 0x03, 0x03, 0x00, 0x00};
uint8_t zoom_in[] = {0xFD, 0x09, 0x01, 0x01, 0x00, 0x2E, 0x00, 0x00, 0x00, 0x00, 0x00};
uint8_t zoom_out[] = {0xFD, 0x09, 0x01, 0x01, 0x00, 0x2D, 0x00, 0x00, 0x00, 0x00, 0x00};
uint8_t ctrl_release[] = {0xFD, 0x09, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
```
# 2021-11-29 backup code and verifications for demo
We finished the assembly of the device, which looks super cool now.
![PCB2](./Pictures/PXL_20211130_164209594.jpg)

Yayati brought a charging unit for the battery and we soldered it on the power pins on the device and we can just plugged in the battery to the charging unit instead. This way the user would not need to unplug the battery to charge it every time. The user just need to charge the charging unit.
```
if(abs(a1[1])>=50) {
  mouse[4] = 5*(a1[1]/abs(a1[1]));
}
else {
  mouse[4] = 0;
}
if(abs(a1[0])>=50) {
    mouse[5] = -5*(a1[0]/abs(a1[0]));
}
else {
    mouse[5] = 0;
}
```
We also prepared a backup version of the code. Since the device only works when our hand is flat, sometimes it is difficult to maneuver it. We only changed the cursor movement part of code and we are only using the acceleration values we get from tilting our hand. Therefore, this mouse will be used like a wireless joystick instead.

```
if ((mouse[4] > 0 && old_mouseX < 0) || (mouse[4] < 0 && old_mouseX > 0)) {
  mouse[4] = 0;
}
else {
  old_mouseX = mouse[4];
}
if ((mouse[5] > 0 && old_mouseY < 0) || (mouse[5] < 0 && old_mouseY > 0)) {
    mouse[5] = 0;
}
else {
  old_mouseY = mouse[5];
}
```
Yayti and I also worked on updating the algorithms. We usually got this error that whenever our cursor moves to a direction and when it stops, it will move backwards a bit. These few lines will ensure that our mouse will go in one direction and do not go the opposite direction unless it fully stops.

We finished off the project by performing various verifications that our design documents dictates. Unsurprisingly, they all passed because our project as a whole works. All the verifications can be checked under demo_videos in the git repo.

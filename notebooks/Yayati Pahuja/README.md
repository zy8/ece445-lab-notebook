# Yayati ECE 445 Notebook

[[_TOC_]]

# 2021-09-19 - Parts order

We ordered some sensors and the STM32 development board so that we can start testing parts this week:
1. Gyroscope (https://www.digikey.com/product-detail/en/stmicroelectronics/STEVAL-MKI136V1/497-14894-ND/4901476)
2. Accelerometer (https://www.digikey.com/product-detail/en/analog-devices-inc/EVAL-ADXL335Z/EVAL-ADXL335Z-ND/1995483)
3. Arm Cortex M3 development board (https://www.digikey.com/product-detail/en/stmicroelectronics/NUCLEO-L152RE/497-14363-ND/4695528)

# 2021-09-21 - Discussion with Feiyu

During the weekly meeting with Feiyu, we were advised to find an alternative solution for our mouse drivers since building custom drivers for Windows is quite an hefty task and might not be doable within the given development time frame. As a result, I started looking at alternative solutions which mainly relied on finding  open source drivers that could be utilized with our current microcontroller and BT module.
Potention solution: https://github.com/T-vK/ESP32-BLE-Mouse using the ESP32 chip as the bluetooth module. However, would need to check compatibility of the library with STCube since the given repo uses Arduino IDE.

# 2021-09-25 - Part update

Found a new bluetooth module that supports an HID profile.
https://www.digikey.com/en/products/detail/microchip-technology/RN42HID-I-RM/3597485
I watched a few online tutorials for this module and found that it can be programmed to act as an HID mouse device that would remove the need for any custom drivers. Instead, we can send HID raw reports through the module that will act as the mouse input.

# 2021-09-26 - Circuit schematic

Worked on the circuit schematic. Main focus on MCU and BT module connections while referencing the datasheets. Since the BT module uses UART communication, we use the PA0-PA3 UART pins to make connections with the UART pins on the BT module.

![](BT-schematic.png)
![](MCU-schematic.png)

# 2021-10-06 - Part update, Schematic update, PCB v1

## Parts
The following part updates were made due to unavailability
1. Switched voltage regulators from LP590X to LM1117 series
2. Switched accelerometer from ADXL334Z to ADXL343

## Schematic v1
The updated schematic with the new parts:

![](schematicv1.png)

## PCB v1
Designed the following PCB after the review for first order:

![](PCBv1.png)

# 2021-10-09 - Accelerometer algorithm

Once Jeff configured the SPI communication between the accelerometer and the MCU, a basic algorithm was written to calculate the delta x and delta y for the mouse using double integration on the data obtained from the accelerometer. Performed using the evaluation board for the sensor and the development board. 
Algorithm: Read the acceleration values during each loop, calculate current velocity(1st integration), calculate distance moved(2nd integration).
```
while (1)
{
  adxl_read(0x32);
  x = ((data[1]<<8)|data[0]);
  y = ((data[3]<<8)|data[2]);
  z = ((data[5]<<8)|data[4]);
  a1[0] = x*.0078;
  a1[1] = y*.0078;
  a1[2] = z*.0078;

  for(int i=0; i<3; i++){
    v1[i] = v0[i] + a0[i] + ((a1[i] - a0[i])/2);
    delx[i] = v0[i] + ((v1[i] - v0[i])/2);
    a0[i] = a1[i];
  }
  printf("\r\ndata: %.2f %.2f %.2f", delx[0], delx[1], delx[2]);
  uint8_t res = adxl_read_byte(0x30);
  int tap = HAL_GPIO_ReadPin(IN1_GPIO_Port, IN1_Pin);
  if(tap){
    printf("\r\ntap");
  }
}
```

# 2021-10-18 - BT module setup

The bluetooth module needs to be setup in the HID mode to act as a peripheral device. To switch the module into HID mode, we need to power on the module using a 3.3V power supply and connect it to a master device such as a PC. A serial communication application is also needed to program the module. Once the the module is powered on and connected to the PC, send the following commands through the serial communication port:
```
$$$ // To enter the command mode
SH,0230 // To switch the device into combo mode, which lets it act as both a mouse and a keyboard
S~,6 // To enable the HID profile. This makes the module appear as an input device in the BT discovery menu on a PC and allows us to send HID reports for mouse control
R,1 // To rebooth the module
```
Tried connecting to the module through a PC again and it successfully showed up as an input device.
After the setup, we connected the module to the UART RX/TX pins on the development board as shown in the schematics above.
Now we can send raw HID reports from the development board to a PC through the BT module. The HID report is as follows:

![](hid_format.png)

We decided to use this in conjunction with our accelerometer algorithm using the following code:
```
uint8_t mouse[] = {0xFD, 0x05, 0x02, 0x00, delx[0], delx[1], 0x00}; // HID raw report format
HAL_UART_Transmit(&huart1,mouse,sizeof(mouse),10); // send the report to the UART1 pin of the dev board
```
Using this code we were able to move the cursor by moving the accelerometer. However, the movement was quite glitchy, so we need to improve our algorithm.

# 2021-10-24 - Gyroscope setup

We received the PCBs today. However, we still don't have the MCU chip so we decided to test the gyroscope first. We soldered the gyroscope on the smaller PCB using the respective stencil and solder paste in order to test it with the dev board. Since the gyroscope also communicates using SPI, the setup with the dev board was pretty similar to the accelerometer. Once the SPI setup was done, we implemented the following code to read data from the gyroscope to generate a click signal.
```
gyro_read(0x43);
x = ((data[0]<<8)|data[1]);
y = ((data[2]<<8)|data[3]);
if (x >= 11000 ){
  click_flag = 1;
}
if (x <= -10000 ){
  release_flag = 1;
}
if (click_flag){
  cx = snprintf(Test, 50, "\r\n \r\nclick!");
  HAL_UART_Transmit(&huart2,Test,cx,10);
  click_flag = 0;
}
if (release_flag){
  cx = snprintf(Test, 50, "\r\n\r\nrelease!");
  HAL_UART_Transmit(&huart2,Test,cx,10);
  release_flag = 0;
}
HAL_Delay(200);
```
With this code we were able to generate a click signal by giving a small jerk to the gyroscope PCB since it was still sonnected to the dev board using mounting cables and we couldn't move it too much or the connection would break. We also saw that when we removed the 200ms delay, there were duplicate click signals that were being generated with a single jerk due to the MCU being quite fast. To circumvent this problem, we need to implement a manual timer that will be activated after a click during which another click won't be registered.

# 2021-10-31 - Slow week

This week we're mostly just waiting on all parts to arrive so that we can assemble the first PCB. Jeff worked on adding the gyroscope timer so that there is at least a 0.2 seconds of delay between consecutive clicks in order to avoid dublicate clicks in a single gyro jerk.

# 2021-11-07 - PCB debugging

While assembling the PCB we noticed that our whole board was getting shorted due to a design problem.

![](footprint3.jfif)

In the image above, the power net line passes through the mounting holes for the gyroscope connectors heads, which basically connects all the PCB layers in this particular area. In this header, the pin 2 is connected to GND as well which now comes in contact with the power net because of the mounting holes so the whole circuit is shorted.
To at least get something working, we manually disconnected the power net at the top and the bottom of these connectors by scratching the power net layer on the backside of the PCB and connected the battery directly to the regulator on the board using connecting wires.

# 2021-11-08 - PCB v2

We needed a new PCB design because of the issue with the version 1 of the PCB. We also decided to make the main PCB and the gyroscope PCB a lot smaller this time around by using smaller connector headers and removing some unneeded parts from the main PCB. These were the final designs:

![](PCBv2.png)

![](gyrov2.png)

# 2021-11-14 - Acceleration algorithm

This week we didn't do much since we were waiting on the new PCB to arrive. I worked with Zhiyuan on the acceleration algorithm a little bit using the evaluation board and the dev kit to reduce some more noise from the data.

# 2021-11-21 - Acceleration algorithm cont.

We were able to imporove the acceleration data a little more by using a 127 value average in each loop instead of just using the instantaneous acceleration values each loop since that was throwing off the velocity and position calculation even with a slight erroneous/unexpected value from the sensor. Using this makes the directional analysis for the movement a lot more stable. See the code below:

```
temp1[0] = ((data[1]<<8)|data[0]);
temp1[1] = ((data[3]<<8)|data[2]);
temp1[2] = ((data[5]<<8)|data[4]);
a1[0] += temp1[0];
a1[1] += temp1[1];
a1[2] += temp1[2];
if(a_avg_count == 127){
    for(int i=0; i<3; i++){
      a1[i] = a1[i] / 128 - offsets[i]; //offset is the calibration offset
    }
}
```

# 2021-11-26 - Putting everything together

We modified the acceleration data a little more where any 3 equal and consecutive velocity values would lead to the velocity becoming 0. This was done due to the fact that the velocities can be non-zero even when acceleration is 0 after a recent hand movement due to the integration calculation being not completely precise. In the code below, v1 is current velocity, v0 is previous, v00 is the one before the previous.

```
if(v1[i]!=0 && v1[i]==v0[i] && v0[i] == v00[i]){
 v1[i] = 0;
}
```

We also added a directional analysis where the cursor can't change directions from left to right or up to down before coming to a stop first through our position calculation. This was done so that any unexpected sign changes in the position change can be ignored to further remove glitches. In the code below, mouse[] is the HID report that is sent at the end, and old_mouseX/Y contain the previous delta values sent.

```
if ((mouse[4] > 0 && old_mouseX < 0) || (mouse[4] < 0 && old_mouseX > 0)) {
  mouse[4] = 0;
}
else {
  old_mouseX = mouse[4];
}
if ((mouse[5] > 0 && old_mouseY < 0) || (mouse[5] < 0 && old_mouseY > 0)) {
    mouse[5] = 0;
}
else {
  old_mouseY = mouse[5];
}
```

Once we were able to precisely measure the hand and finger movements using out gyro and accelerometer algorithms, we starting with the conditions for the advanced functionalities.
The index and the middle fingers separately act as left and right clicks respectively.
The thumb gyro acts as a advanced mode when activated.
In advanced mode:
1) Move index finger up/down to scroll up/down respectively.
2) Tilt the hand up/down to zoom out/in respectively.
3) Tilt the hand right/left to turn volume up/down respectively.

# 2021-11-28 - Backup code for movement

Even though out movement algorithm work pretty well, there are still some occassional glitches. To get rid of these, we decided to create a backup code that would allow the cursor movement using hand tilt instead of movement. This was fairly wasy to implement as we didn't need any integration in this. A tilt in a given direction would basically move the cursor a fixed distance in the same direction in each loop, making the mouse move like it's being controlled by a joystick almost. The code used is as follows:

```
if(abs(a1[1])>=50) {
  mouse[4] = 5*(a1[1]/abs(a1[1]));
}
else {
  mouse[4] = 0;
}
if(abs(a1[0])>=50) {
    mouse[5] = -5*(a1[0]/abs(a1[0]));
}
else {
    mouse[5] = 0;
}
```

# 2021-11-30 - Demo day

This marks the end of the development of the project. Now we will focus on working on the presentation and the final paper.
